﻿using PersonalIndoorFarm.Lib;
using StardewModdingAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.API.QuickSave
{
    internal class QuickSaveHandler
    {
        public static void Initialize()
        {
            Helper.Events.GameLoop.GameLaunched += GameLoop_GameLaunched; ;
        }

        private static void GameLoop_GameLaunched(object sender, StardewModdingAPI.Events.GameLaunchedEventArgs e)
        {
            if (!Helper.ModRegistry.IsLoaded("DLX.QuickSave"))
                return;

            var api = Helper.ModRegistry.GetApi<IQuickSaveAPI>("DLX.QuickSave");
            api.LoadingEvent += Api_LoadingEvent;
        }

        private static void Api_LoadingEvent(object sender, ILoadingEventArgs e)
        {
            if (Context.ScreenId > 0)
                return;

            DayUpdate.RemoveAllPifLocations();
        }
    }
}
