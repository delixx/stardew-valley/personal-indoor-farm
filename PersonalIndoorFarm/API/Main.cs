﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.API
{
    internal class Main
    {
        public static void Initialize()
        {
            GMCM.Initialize();
            QuickSave.QuickSaveHandler.Initialize();
        }
    }
}
