﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    internal class MissingRoomException : Exception
    {
        public static string PID; 
        public MissingRoomException(string pid)
        {
            PID = pid;
        }
    }
}
