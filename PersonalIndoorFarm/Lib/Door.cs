﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PersonalIndoorFarm.Lib.SelectionMenu;
using PersonalIndoorFarm.Lib.Warping;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using StardewValley;
using StardewValley.Locations;
using StardewValley.Menus;
using StardewValley.Objects;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    public class Door
    {
        public const string ItemId = "DLX.PIF_Door";
        public const string QualifiedItemId = "(F)" + ItemId;

        public const string WoodStepSound = "WoodStep";
        public static void Initialize()
        {
            //Working around a Harmony Bug where PlacementPlus also Prefixed Furniture.checkForAction, but this for some reason removed my patch from the result
            Helper.Events.GameLoop.DayStarted += DayStarted;
        }

        private static void DayStarted(object sender, StardewModdingAPI.Events.DayStartedEventArgs e)
        {
            Helper.Events.GameLoop.DayStarted -= DayStarted;
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(Furniture), nameof(Furniture.checkForAction), new[] { typeof(Farmer), typeof(bool) }),
                prefix: new HarmonyMethod(typeof(Door).GetMethod(nameof(checkForAction_Prefix)), priority: Priority.First)
            );
            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(Furniture), nameof(Furniture.draw), new[] { typeof(SpriteBatch), typeof(int), typeof(int), typeof(float) }),
                postfix: new HarmonyMethod(typeof(Door).GetMethod(nameof(draw_Postfix)))
            );
        }
        public static bool checkForAction_Prefix(Furniture __instance, ref bool __result)
        {
            try {
                if (!isDimensionDoor(__instance.ItemId, out var model))
                    return true;

                __result = true;
                checkWarp(model, __instance);
                return false;
            } catch (Exception err) {
                Monitor.LogOnce("Error at Door.checkForAction_Prefix:\n" + err, LogLevel.Error);
                return true;
            }
        }

        public static bool isDimensionDoor(string itemId, out DoorAssetModel model)
        {
            var asset = Helper.GameContent.Load<Dictionary<string, DoorAssetModel>>(AssetRequested.DoorsAsset);
            if (asset.TryGetValue(itemId, out model)) {
                model.DoorId = String.IsNullOrEmpty(model.DoorId) ? itemId : model.DoorId;
                return true;
            }

            model = null;
            return false;
        }

        public static Farmer getOwner(Furniture door)
        {
            var rule = Enum.Parse<DoorOwnerEnum>(
            Game1.currentLocation is FarmHouse ? Config.OwnerFarmhouse : Config.OwnerOutside,
            true);

            if (rule == DoorOwnerEnum.None)
                return null;

            else if (rule == DoorOwnerEnum.Host)
                return Game1.MasterPlayer;

            else if (rule == DoorOwnerEnum.CurrentPlayer)
                return Game1.player;

            else if (rule == DoorOwnerEnum.Owner)
                return Game1.currentLocation is FarmHouse fh && fh.HasOwner ? Game1.GetPlayer(fh.OwnerId) : null;

            else if (rule == DoorOwnerEnum.PlacedBy && door is not null)
                return Game1.GetPlayer(door.owner.Value);

            return null;
        }

        public static Farmer checkOwner(Furniture door, string doorId, string pidKey)
        {
            Farmer owner = getOwner(door);

            if (owner is null) {
                Game1.showRedMessage(Helper.Translation.Get("Door.Empty"));
                return null;
            }

            var isOwner = owner.UniqueMultiplayerID == Game1.player.UniqueMultiplayerID;
            if (isOwner && Game1.player.CurrentItem?.QualifiedItemId == Key.QualifiedItemId) {
                Key.useOnDoor(Game1.player, doorId);
                return null;
            }

            var lockStatus = Key.getDoorLocked(owner, doorId);
            if (!isOwner && lockStatus == DoorLockEnum.Locked) {
                Game1.showRedMessage(Helper.Translation.Get("Door.Locked"));
                return null;
            }

            if (!isOwner && lockStatus == DoorLockEnum.LockedWhenOffline && !Game1.getOnlineFarmers().Any(e => e.UniqueMultiplayerID == owner.UniqueMultiplayerID)) {
                Game1.showRedMessage(Helper.Translation.Get("Door.Offline"));
                return null;
            }

            if (!isOwner && !owner.modData.ContainsKey(pidKey)) {
                Game1.showRedMessage(Helper.Translation.Get("Door.Empty"));
                return null;
            }

            return owner;
        }

        public static void checkWarp(DoorAssetModel doorModel, Furniture door)
        {
            var pidKey = PersonalRoom.generateFarmerPIDKey(doorModel.DoorId);
            var owner = checkOwner(door, doorModel.DoorId, pidKey);

            if (owner is null)
                return;

            var models = Helper.GameContent.Load<Dictionary<string, PersonalRoomModel>>(AssetRequested.FarmsAsset);
            if (!models.Where(room => !room.Value.ExcludeFromRoomSelection).Any()) {
                Monitor.LogOnce("No Content Packs installed message thrown");
                Game1.showRedMessage(Helper.Translation.Get("NoContentPacks"));
                return;
            }

            if (!owner.modData.TryGetValue(pidKey, out var pid)) {
                if (!string.IsNullOrEmpty(doorModel.RoomId)) {
                    pid = doorModel.RoomId;
                    CreateConfirmedModel(pid);

                } else {
                    SelectionMenuBase farmSelection = Config.SelectionMenuStyle == SelectionMenuStyles.Classic.ToString() ? new SelectionMenuClassic() : new SelectionMenuTarot();
                    farmSelection.exitFunction = () => {
                        if (!farmSelection.SelectionConfirmed)
                            return;

                        CreateConfirmedModel(farmSelection.CurrentModel.Key);
                    };

                    Game1.activeClickableMenu = farmSelection;
                    return;

                }

                void CreateConfirmedModel(string pid)
                {
                    owner.modData.Add(pidKey, pid);
                    var location = PersonalRoom.createLocation(pid, Game1.player, doorModel.DoorId);
                    Helper.Multiplayer.SendMessage(new ShareLocationModel(pid, doorModel.DoorId, Game1.player.UniqueMultiplayerID), "shareLocation", new[] { ModManifest.UniqueID });
                    PersonalRoom.setInitialDayAndSeason(location);
                    PersonalRoom.placeInitialItems(location, pid);
                    Helper.GameContent.InvalidateCache(@"Data\Minecarts");
                }
            }


            var model = PersonalRoom.getModel(pid);
            if (model is null) {
                Game1.showRedMessage(Helper.Translation.Get("Door.Empty"));
                return;
            }

            playDoorSound(doorModel);
            setArrivalDirection(owner, model);
            warpFarmerToRoom(owner, pid, model, doorModel);
        }

        private static void warpFarmerToRoom(Farmer owner, string pid, PersonalRoomModel model, DoorAssetModel doorModel)
        {
            var destination = PersonalRoom.generateLocationKey(pid, owner.UniqueMultiplayerID, doorModel.DoorId);
            WarpToFarmHouse.PushLastEntrance(false, destination);
            var arrivalTile = PersonalRoom.getArrivalTile(owner, model);

            Game1.player.warpFarmer(new Warp(0, 0, destination, arrivalTile.X, arrivalTile.Y, false));

            var location = Game1.getLocationFromName(destination); //For debugging :)
        }

        private static void setArrivalDirection(Farmer owner, PersonalRoomModel model)
        {
            if (PersonalRoom.getArrivalDirection(owner, model) is int arrivalDirection) {

                if (Helper.ModRegistry.IsLoaded("no.warp.delay"))
                    Game1.player.faceDirection(arrivalDirection);
                else
                    WarpToFarmHouse.DirectionAfterFadeIn.Value = arrivalDirection;
            }
        }

        private static void playDoorSound(DoorAssetModel doorModel)
        {
            if (doorModel.Sound is null)
                doorModel.Sound = "doorOpen";

            if (doorModel.Sound == "")
                return;

            else if (doorModel.Sound == WoodStepSound) {
                Game1.currentLocation.playSound("woodyStep", Game1.player.Tile);
                DelayedAction.playSoundAfterDelay("woodyStep", 300, Game1.currentLocation, Game1.player.Tile);
                DelayedAction.playSoundAfterDelay("woodyStep", 600);

            } else
                Game1.currentLocation.playSound(doorModel.Sound, Game1.player.Tile);
        }

        public static List<string> getDoorIds(Farmer who)
        {
            var ret = new List<string>();
            var startKey = PersonalRoom.BaseFarmerPidKey + "_";

            foreach (var e in who.modData.Pairs) {
                if (!e.Key.StartsWith(startKey))
                    continue;

                ret.Add(e.Key.Substring(startKey.Length));
            }

            return ret;
        }

        public static void draw_Postfix(Furniture __instance, SpriteBatch spriteBatch, int x, int y, float alpha = 1f)
        {
            if (Game1.player.CurrentItem?.QualifiedItemId != Key.QualifiedItemId)
                return;

            if (!isDimensionDoor(__instance.ItemId, out var doorModel))
                return;

            var owner = getOwner(__instance);

            if (owner is null || owner.UniqueMultiplayerID != Game1.player.UniqueMultiplayerID)
                return;

            Key.drawOverlay(spriteBatch, __instance, doorModel.DoorId, owner);
        }
    }
}
