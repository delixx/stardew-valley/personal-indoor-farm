﻿using Microsoft.Xna.Framework;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib.Warping
{
    public sealed class Entrance
    {
        public string Location;
        public bool IsStructure;
        public Vector2 Position;
        public bool WasMinecartEntry;
        public Entrance(bool wasMinecartEntry) : this(Game1.player.currentLocation.NameOrUniqueName, Game1.player.currentLocation.isStructure.Value, Game1.player.Position, wasMinecartEntry) { }
        public Entrance(string location, bool isStructure, Vector2 position, bool wasMinecartEntry)
        {
            Location = location;
            IsStructure = isStructure;
            Position = position;
            WasMinecartEntry = wasMinecartEntry;
        }
        public Entrance() { }
    }
}
