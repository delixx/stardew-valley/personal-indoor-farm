﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using StardewValley;
using StardewValley.Mods;
using StardewValley.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib.Warping
{
    public sealed class WarpToFarmHouse
    {
        public static PerScreen<bool> Exiting_PIF = new();
        public static Dictionary<string, Entrance> LastEntranceDictionary => ModData.GetInstance().LastEntranceDictionaryForPlayer(Game1.player.UniqueMultiplayerID);
        public static OrderedStack<Entrance> LastEntranceStack => ModData.GetInstance().LastEntranceStackForPlayer(Game1.player.UniqueMultiplayerID);

        public static PerScreen<int?> DirectionAfterFadeIn = new();

        public static void Initialize()
        {
            GameLocation.RegisterTouchAction("DLX.PIF_WarpToFarmHouse", HandleWarpToFarmHouse);
            //I had to rename it to 'Door' because the game does a stupid .contains "Warp" check and throws a pointless warning 
            GameLocation.RegisterTileAction("DLX.PIF_DoorToFarmHouse", HandleWarpToFarmHouseAction);

            GameLocation.RegisterTouchAction("DLX.PIF_WarpSameLocation", HandleWarpSameLocation);

            GameLocation.RegisterTileAction("DLX.PIF_DoorSameLocation", HandleDoorSameLocation);

            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(GameLocation), nameof(GameLocation.resetForPlayerEntry)),
                prefix: new HarmonyMethod(typeof(WarpToFarmHouse), nameof(WarpToFarmHouse.SetWarpingPosition))
            );

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(ModHooks), nameof(ModHooks.OnGameLocation_ResetForPlayerEntry), new[] { typeof(GameLocation), typeof(Action) }),
                finalizer: new HarmonyMethod(typeof(WarpToFarmHouse), nameof(WarpToFarmHouse.SetWarpingPosition))
            );

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(Game1), "onFadeToBlackComplete"),
                finalizer: new HarmonyMethod(typeof(WarpToFarmHouse), nameof(WarpToFarmHouse.onFadeToBlackComplete_Finalizer))
            );

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(Game1), "onFadedBackInComplete"),
                finalizer: new HarmonyMethod(typeof(WarpToFarmHouse), nameof(WarpToFarmHouse.onFadedBackInComplete_Finalizer))
            );

            Helper.Events.Multiplayer.ModMessageReceived += ModMessageReceived;
        }

        private static bool HandleWarpToFarmHouseAction(GameLocation location, string[] args, Farmer player, Point tile)
        {
            Game1.currentLocation.playSound("doorOpen", Game1.player.Tile);
            HandleWarpToFarmHouse(location, args, player, new Vector2());
            return true;
        }

        public static void HandleWarpToFarmHouse(GameLocation location, string[] args, Farmer player, Vector2 tile)
        {
            var entrance = PopLastSafeEntrance(location);

            if(entrance is null) {
                BedFurniture.ApplyWakeUpPosition(player);
                return;
            }

            LastEntranceStack.Push(entrance);

            var xTile = (int)Math.Round(entrance.Position.X / 64);
            var yTile = (int)Math.Round(entrance.Position.Y / 64);
            Game1.warpFarmer(entrance.Location, xTile, yTile, 2, true);
            Game1.xLocationAfterWarp = xTile;
            Game1.yLocationAfterWarp = yTile;

            if (Helper.ModRegistry.IsLoaded("no.warp.delay")) {
                LastEntranceStack.Pop();
                Game1.player.setTileLocation(new(xTile, yTile));
                var l = Game1.getLocationFromName(entrance.Location);
                Game1.currentLocation = l;
                Game1.player.Position = new Vector2(entrance.Position.X, entrance.Position.Y);
                LastEntranceChanged();

            } else
                Exiting_PIF.Value = true;
        }

        private static Entrance PopLastSafeEntrance(GameLocation location)
        {
            Entrance entrance = null;

            if (LastEntranceStack.Any())
                entrance = LastEntranceStack.Pop();

            //Entering through a door has higher priority
            if (entrance?.WasMinecartEntry != false
                && location is not null
                && LastEntranceDictionary.TryGetValue(location.NameOrUniqueName, out var e2)
                && !e2.WasMinecartEntry)
                entrance = e2;

            //Stack contains faulty/removed entries. Trying to recover to next best entrance
            while (Game1.getLocationFromName(entrance?.Location) is null && LastEntranceStack.Any())
                entrance = LastEntranceStack.Pop();

            return entrance;
        }

        public static void onFadedBackInComplete_Finalizer()
        {
            if (Exiting_PIF.Value)
                LastEntranceStack.Pop();

            Exiting_PIF.Value = false;
            LastEntranceChanged();
        }

        public static void onFadeToBlackComplete_Finalizer()
        {
            if (DirectionAfterFadeIn.Value is int direction) {
                Game1.player.faceDirection(direction);
                DirectionAfterFadeIn.Value = null;
            }

            SetWarpingPosition();
        }

        public static void SetWarpingPosition()
        {
            if (!Exiting_PIF.Value)
                return;

            var entrance = LastEntranceStack.Peek();
            Game1.player.Position = entrance.Position;
        }

        private static bool HandleDoorSameLocation(GameLocation location, string[] args, Farmer player, Point tile)
        {
            Game1.currentLocation.playSound("doorOpen", Game1.player.Tile);
            HandleWarpSameLocation(location, args, player, new Vector2());
            return true;
        }
        public static void HandleWarpSameLocation(GameLocation location, string[] args, Farmer player, Vector2 tile)
        {
            if (args.Length < 3) {
                Monitor.LogOnce("Missing coordinates for WarpSameLocation. Expected WarpSameLocation <X> <Y>", LogLevel.Warn);
                return;
            }

            if (!int.TryParse(args[1], out var x)) {
                Monitor.LogOnce("X Coordinate of WarpSameLocation did not parse to number", LogLevel.Warn);
                return;
            }

            if (!int.TryParse(args[2], out var y)) {
                Monitor.LogOnce("Y Coordinate of WarpSameLocation did not parse to number", LogLevel.Warn);
                return;
            }

            var facingDirection = Game1.player.FacingDirection;
            if (args.Length > 3 && int.TryParse(args[3], out var dir))
                facingDirection = dir;

            Game1.warpFarmer(new LocationRequest("", false, location), x, y, facingDirection);
        }

        public static void PushLastEntrance(bool wasMinecartEntry, string destination)
        {
            if (!Game1.currentLocation.NameOrUniqueName.StartsWith(PersonalRoom.BaseLocationKey) || LastEntranceStack is null)
                LastEntranceStack.Clear();

            var entrance = new Entrance(wasMinecartEntry);
            LastEntranceStack.Push(entrance);

            if (!LastEntranceDictionary.TryGetValue(destination, out var lastEntrance)
                    || !entrance.WasMinecartEntry
                    || !entrance.WasMinecartEntry && !lastEntrance.WasMinecartEntry)
                LastEntranceDictionary[destination] = entrance;

            LastEntranceChanged();
        }

        public static void LastEntranceChanged()
        {
            if (Context.IsOnHostComputer)
                return;

            Helper.Multiplayer.SendMessage(new KeyValuePair<Dictionary<string, Entrance>, OrderedStack<Entrance>>(LastEntranceDictionary, LastEntranceStack), "LastEntranceChanged", new[] { ModManifest.UniqueID }, new[] { Game1.MasterPlayer.UniqueMultiplayerID });
        }

        private static void ModMessageReceived(object sender, StardewModdingAPI.Events.ModMessageReceivedEventArgs e)
        {
            if (!Context.IsMainPlayer)
                return;

            if (e.Type != "LastEntranceChanged")
                return;

            Monitor.Log($"Host received LastEntranceChanged from player: {e.FromPlayerID}", DebugLogLevel);

            var messageData = e.ReadAs<KeyValuePair<Dictionary<string, Entrance>, OrderedStack<Entrance>>>();
            var modData = ModData.GetInstance();

            var playerEntranceDictionary = messageData.Key;
            modData.LastEntranceDictionary[e.FromPlayerID] = playerEntranceDictionary;

            var playerEntranceStack = messageData.Value;
            modData.LastEntranceStack[e.FromPlayerID] = playerEntranceStack;

        }
    }
}
