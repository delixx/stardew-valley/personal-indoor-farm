﻿using Microsoft.Xna.Framework.Graphics;
using StardewValley;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib.SelectionMenu
{
    internal abstract class SelectionMenuBase : IClickableMenu
    {
        protected Dictionary<string, PersonalRoomModel> Models;

        protected Texture2D Preview;
        public KeyValuePair<string, PersonalRoomModel> CurrentModel { get; set; } = new();
        public bool SelectionConfirmed = false;

        protected void AssignPreview()
            => Preview = CurrentModel.Value?.Preview is null ? null : Helper.GameContent.Load<Texture2D>(CurrentModel.Value.Preview);

        protected void AssignModels()
            => Models = Helper.GameContent.Load<Dictionary<string, PersonalRoomModel>>(AssetRequested.FarmsAsset)
                .Where(room => !room.Value.ExcludeFromRoomSelection)
                .ToDictionary(x => x.Key, x => x.Value);

        public SelectionMenuBase() : base()
        {

        }
        public SelectionMenuBase(int x, int y, int width, int height, bool showUpperRightCloseButton = false) : base(x, y, width, height, showUpperRightCloseButton)
        {
        }
    }
}
