﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewModdingAPI;
using StardewValley;
using StardewValley.BellsAndWhistles;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib.SelectionMenu
{
    internal class SelectionMenuClassic : SelectionMenuBase
    {

        protected int _index;
        protected int Index { get => _index; set { _index = value; CurrentModel = Models.ElementAt(Index); } }

        public ClickableTextureComponent LeftArrow;
        public ClickableTextureComponent RightArrow;
        public ClickableTextureComponent ConfirmButton;
        public ClickableTextureComponent CancelButton;

        public SelectionMenuClassic() : base()
        {
            AssignModels();
            CurrentModel = Models.First();
            AssignPreview();
            UpdateScreenProportions();
            CreateClickTableTextures();
        }

        private void CreateClickTableTextures()
        {
            var buttonSpacing = 16;
            var arrowSpacing = 40;

            var buttonSize = 64;

            var arrowWidth = 48;
            var arrowHeight = 44;

            LeftArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width / 2 - buttonSpacing - buttonSize - arrowWidth - arrowSpacing, yPositionOnScreen + height - 100, arrowWidth, arrowHeight), Game1.mouseCursors, new Rectangle(352, 495, 12, 11), arrowWidth / 12f) {
                myID = 8000,
                rightNeighborID = 8001,
                downNeighborID = 8003,
            };

            RightArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width / 2 + buttonSpacing + buttonSize + arrowSpacing, yPositionOnScreen + height - 100, arrowWidth, arrowHeight), Game1.mouseCursors, new Rectangle(365, 495, 12, 11), arrowWidth / 12f) {
                myID = 8001,
                leftNeighborID = 8000,
                downNeighborID = 8004
            };

            CancelButton = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width / 2 - buttonSize - buttonSpacing, yPositionOnScreen + height - 60, buttonSize, buttonSize), Game1.mouseCursors, new Rectangle(192, 256, 64, 64), buttonSize / 64f) {
                myID = 8003,
                upNeighborID = 8000,
                rightNeighborID = 8004
            };

            ConfirmButton = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width / 2 + buttonSpacing, yPositionOnScreen + height - 60, buttonSize, buttonSize), Game1.mouseCursors, new Rectangle(128, 256, 64, 64), buttonSize / 64f) {
                myID = 8004,
                upNeighborID = 8001,
                leftNeighborID = 8003
            };
        }

        public override void draw(SpriteBatch b)
        {
            b.Draw(Game1.fadeToBlackRect, new Rectangle(0, 0, Game1.uiViewport.Width, Game1.uiViewport.Height), Color.Black * 0.5f);
            var textureBoxRectangle = new Rectangle(xPositionOnScreen, yPositionOnScreen + 64, width, height - 200);
            drawTextureBox(b, Game1.mouseCursors, new Rectangle(384, 373, 18, 18), textureBoxRectangle.X, textureBoxRectangle.Y, textureBoxRectangle.Width, textureBoxRectangle.Height, Color.White, 4f);

            if (Preview is not null) {
                Rectangle previewRectangle = new Rectangle(textureBoxRectangle.X + 16, textureBoxRectangle.Y + 18, textureBoxRectangle.Width - 32, textureBoxRectangle.Height - 32);
                b.Draw(Preview, previewRectangle, Color.White);
            }

            if (canPageLeft())
                LeftArrow.draw(b);
            if (canPageRight())
                RightArrow.draw(b);
            CancelButton.draw(b);
            ConfirmButton.draw(b);

            if (CurrentModel.Value?.DisplayName is not null)
                SpriteText.drawStringWithScrollCenteredAt(b, CurrentModel.Value.DisplayName, xPositionOnScreen + width / 2, yPositionOnScreen + height - 180);

            drawMouse(b);
        }

        public override void receiveLeftClick(int x, int y, bool playSound = true)
        {
            base.receiveLeftClick(x, y);

            if (LeftArrow.containsPoint(x, y) && canPageLeft()) {
                Index--;
                Game1.playSound("shwip");
                AssignPreview();

            } else if (RightArrow.containsPoint(x, y) && canPageRight()) {
                Index++;
                Game1.playSound("shwip");
                AssignPreview();

            } else if (CancelButton.containsPoint(x, y))
                exitThisMenu(true);

            else if (ConfirmButton.containsPoint(x, y)) {
                SelectionConfirmed = true;
                Game1.playSound("coin");
                exitThisMenu(false);
            }
        }

        public override void gameWindowSizeChanged(Rectangle oldBounds, Rectangle newBounds)
        {
            UpdateScreenProportions();
            CreateClickTableTextures();
        }

        private void UpdateScreenProportions()
        {
            width = 1000 + borderWidth * 2;
            height = 600 + borderWidth * 2;
            xPositionOnScreen = Game1.uiViewport.Width / 2 - (1000 + borderWidth * 2) / 2;
            yPositionOnScreen = Game1.uiViewport.Height / 2 - (600 + borderWidth * 2) / 2;
        }

        public override void performHoverAction(int x, int y)
        {
            LeftArrow.tryHover(x, y, 0.5f);
            RightArrow.tryHover(x, y, 0.5f);
            CancelButton.tryHover(x, y, 0.15f);
            ConfirmButton.tryHover(x, y, 0.15f);
        }

        private bool canPageLeft()
            => Index > 0;

        private bool canPageRight()
            => Index < Models.Count - 1;
    }
}
