﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using StardewValley;
using StardewValley.BellsAndWhistles;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib.SelectionMenu
{
    internal class SelectionMenuTarot : SelectionMenuBase
    {
        public enum ButtonStyles
        {
            Default,
            Accept,
            ClosedGroup,
            ExpandedGroup
        }

        public class TarotListNode : ClickableComponent
        {
            public KeyValuePair<string, PersonalRoomModel> RoomModel;
            public List<TarotListNode> Children = new();
            public ButtonStyles Style
            {
                get {
                    if (Children.Any())
                        return IsExpanded ? ButtonStyles.ExpandedGroup : ButtonStyles.ClosedGroup;

                    return ButtonStyles.Default;
                }
            }

            public bool IsExpanded;

            public TarotListNode(Rectangle bounds, string name) : base(bounds, name)
            {

            }
            public TarotListNode(Rectangle bounds, string name, KeyValuePair<string, PersonalRoomModel> roomModel) : base(bounds, name)
            {
                RoomModel = roomModel;
            }
        }

        private Texture2D BackgroundTexture;

        public List<TarotListNode> VisibleNodes = new();
        private List<TarotListNode> _nodes = new();
        public ClickableComponent AcceptButton;
        private int _scrollIndex = 0;
        private int _lineHeight = 76;
        private int _indentWidth = 20;
        private int _maxListElements = 7;
        private int _initalOffset = 88;

        private int _mouseX;
        private int _mouseY;

        private int _expandedNodesCount = 0;
        private int _totalNodesCount = 0;
        private bool _hoveringButton = false;

        private ClickableTextureComponent _scrollbarRunner;
        private Rectangle _scrollbarBounds;
        private bool _scrolling;
        private Rectangle _scrollUpButton;
        private Rectangle _scrollDownButton;

        private int _currentlySnappedNode;

        public SelectionMenuTarot() : base()
        {
            BackgroundTexture = Helper.ModContent.Load<Texture2D>(@"assets\SelectionMenu.png");
            AssignModels();
            UpdateScreenProportions();

            initializeUpperRightCloseButton();

            Models = Models.OrderBy(e => string.IsNullOrWhiteSpace(e.Value.Group) ? e.Value.DisplayName ?? e.Key : e.Value.Group).ToDictionary(x => x.Key, x => x.Value);
            AssignNodes();
            ResetClickableRegions();

            var first = _nodes.First();
            CurrentModel = first.Children.Any() ? first.Children.First().RoomModel : first.RoomModel;
            AssignPreview();

            populateClickableComponentList();
            currentlySnappedComponent = VisibleNodes.First();
            if (Game1.options.SnappyMenus)
                snapCursorToCurrentSnappedComponent();
        }

        private void ResetClickableRegions()
        {
            AssignVisibleNodesTree();
            ResetScrollBar();

            AcceptButton = new(new Rectangle(xPositionOnScreen + 180 * 4, yPositionOnScreen + 129 * 4 + 16 + 4, 120 * 4, 23 * 3), "", "") {
                myID = 1000,
                leftNeighborID = -7777
            };

            upperRightCloseButton.bounds.X = xPositionOnScreen + width - 24 - upperRightCloseButton.bounds.Width;
            upperRightCloseButton.bounds.Y = yPositionOnScreen - 8;
        }

        private void ResetScrollBar()
        {
            _scrollUpButton = new Rectangle(xPositionOnScreen, yPositionOnScreen, 8 * 4, 7 * 4);
            _scrollbarBounds = new Rectangle(xPositionOnScreen, _scrollUpButton.Bottom, 8 * 4, height - 2 * _scrollUpButton.Height);
            _scrollDownButton = new Rectangle(xPositionOnScreen, _scrollbarBounds.Bottom, 8 * 4, _scrollUpButton.Height);
            _scrollbarRunner = new ClickableTextureComponent(new Rectangle(_scrollbarBounds.X + 4, _scrollbarBounds.Y + 8, 24, 40), BackgroundTexture, new Rectangle(42, 195, 6, 10), 4f);

            SetScrollBarToCurrentIndex();
        }

        private void SetScrollBarToCurrentIndex()
        {
            if (_scrollIndex == 0) {
                _scrollbarRunner.bounds.Y = _scrollbarBounds.Y;
                return;
            }

            if (_expandedNodesCount <= _maxListElements) {
                _scrollbarRunner.bounds.Y = _scrollbarBounds.Bottom - _scrollbarRunner.bounds.Height;
                return;
            }

            _scrollbarRunner.bounds.Y = (int)((float)(_scrollbarBounds.Height - _scrollbarRunner.bounds.Height) / Math.Max(1, _expandedNodesCount - _maxListElements) * _scrollIndex) + _scrollbarBounds.Y;
            if (_scrollIndex >= _expandedNodesCount - _maxListElements) {
                _scrollbarRunner.bounds.Y = _scrollbarBounds.Bottom - _scrollbarRunner.bounds.Height;
            }
        }

        private void AssignNodes()
        {
            TarotListNode groupHead = null;
            foreach (var model in Models) {
                if (!string.IsNullOrWhiteSpace(model.Value.Group) && (groupHead is null || groupHead.name != model.Value.Group)) {
                    groupHead = new TarotListNode(new(), model.Value.Group);
                    _nodes.Add(groupHead);

                    _totalNodesCount++;
                } else if (string.IsNullOrWhiteSpace(model.Value.Group)) {
                    groupHead = null;
                }

                var entry = new TarotListNode(new(), model.Value.DisplayName ?? model.Key, model);
                if (groupHead is null)
                    _nodes.Add(entry);
                else
                    groupHead.Children.Add(entry);

                _totalNodesCount++;
            }

            _expandedNodesCount = _nodes.Count;
        }

        private void AssignVisibleNodesTree()
        {
            VisibleNodes.Clear();

            int scrollOffset = -_scrollIndex;
            int yOffset = _initalOffset;
            AssignVisibleNodesList(_nodes, ref scrollOffset, ref yOffset);

            populateClickableComponentList();
            if (_currentlySnappedNode >= VisibleNodes.Count)
                _currentlySnappedNode = VisibleNodes.Count - 1;

            currentlySnappedComponent = VisibleNodes[_currentlySnappedNode];
        }

        private void AssignVisibleNodesList(List<TarotListNode> nodes, ref int scrollOffset, ref int yOffset)
        {
            foreach (var node in nodes) {
                if (scrollOffset >= _maxListElements)
                    break;

                AssignVisibleNodesNode(node, ref scrollOffset, ref yOffset);
            }
        }

        private void AssignVisibleNodesNode(TarotListNode node, ref int scrollOffset, ref int yOffset)
        {
            if (scrollOffset >= _maxListElements)
                return;

            if (scrollOffset++ >= 0)
                AddVisibleNode(node, ref yOffset);

            if (node.IsExpanded)
                AssignVisibleNodesList(node.Children, ref scrollOffset, ref yOffset);
        }

        private void AddVisibleNode(TarotListNode node, ref int yOffset)
        {

            var isIndent = !string.IsNullOrWhiteSpace(node.RoomModel.Value?.Group);

            node.bounds = isIndent
                ? new Rectangle(xPositionOnScreen + _indentWidth + 22 * 4, yPositionOnScreen + yOffset, 120 * 4 - _indentWidth, 23 * 3)
                : new Rectangle(xPositionOnScreen + 22 * 4, yPositionOnScreen + yOffset, 120 * 4, 23 * 3);

            yOffset += _lineHeight;

            node.myID = VisibleNodes.Count;
            node.rightNeighborID = 1000;
            node.upNeighborID = -7777;
            node.downNeighborID = -7777;

            VisibleNodes.Add(node);
        }

        private void UpdateScreenProportions()
        {
            width = 1280;
            height = 720;
            xPositionOnScreen = Game1.uiViewport.Width / 2 - width / 2;
            yPositionOnScreen = Game1.uiViewport.Height / 2 - height / 2;
        }


        private void ScrollUp()
        {
            _scrollIndex--;
            AssignVisibleNodesTree();
            SetScrollBarToCurrentIndex();
        }

        private void ScrollDown()
        {
            _scrollIndex++;
            AssignVisibleNodesTree();
            SetScrollBarToCurrentIndex();
        }

        public override void gameWindowSizeChanged(Rectangle oldBounds, Rectangle newBounds)
        {
            UpdateScreenProportions();
            ResetClickableRegions();
        }

        public override void receiveLeftClick(int x, int y, bool playSound = true)
        {
            foreach (var node in VisibleNodes) {
                if (node.containsPoint(x, y)) {
                    NodeClicked(node);
                    break;
                }
            }

            if (AcceptButton.bounds.Contains(x, y)) {
                SelectionConfirmed = true;
                Game1.playSound("select");
                exitThisMenu(false);
            } else if (_scrollbarBounds.Contains(x, y) && IsScrollbarDrawn() && (CanScrollDown() || CanScrollUp()))
                _scrolling = true;
            else if (_scrollUpButton.Contains(x, y) && CanScrollUp()) {
                ScrollUp();
                Game1.playSound("shwip");
            } else if (_scrollDownButton.Contains(x, y) && CanScrollDown()) {
                ScrollDown();
                Game1.playSound("shwip");
            }

            base.receiveLeftClick(x, y, playSound);
        }

        public override void releaseLeftClick(int x, int y)
        {
            base.releaseLeftClick(x, y);
            _scrolling = false;
        }

        public override void receiveScrollWheelAction(int direction)
        {
            if (!IsScrollbarDrawn())
                return;

            if (direction > 0 && CanScrollUp()) {
                ScrollUp();
                Game1.playSound("shiny4");
            } else if (direction < 0 && CanScrollDown()) {
                ScrollDown();
                Game1.playSound("shiny4");
            }
        }

        public override void leftClickHeld(int x, int y)
        {
            if (_scrolling) {
                int oldY = _scrollbarRunner.bounds.Y;
                _scrollbarRunner.bounds.Y = Math.Min(_scrollbarRunner.bounds.Bottom, Math.Max(y, _scrollbarBounds.Top));
                float percentage = (float)(y - _scrollbarBounds.Y) / (float)_scrollbarBounds.Height;
                var maxScrollIndex = Math.Max(0, _expandedNodesCount - _maxListElements);
                _scrollIndex = Math.Clamp((int)Math.Round((double)maxScrollIndex * percentage), 0, maxScrollIndex);
                SetScrollBarToCurrentIndex();
                if (oldY != _scrollbarRunner.bounds.Y) {
                    Game1.playSound("shiny4");
                    AssignVisibleNodesTree();
                }
            }
        }

        protected override void customSnapBehavior(int direction, int oldRegion, int oldID)
        {
            switch (direction) {
                case 0:
                    if (_currentlySnappedNode > 0) {
                        _currentlySnappedNode--;
                        currentlySnappedComponent = VisibleNodes[_currentlySnappedNode];
                        snapCursorToCurrentSnappedComponent();

                    } else if (CanScrollUp()) {
                        ScrollUp();
                    }

                    break;
                case 1:
                    SnapToAcceptButton();
                    break;

                case 2:
                    if (_currentlySnappedNode < VisibleNodes.Count - 1) {
                        _currentlySnappedNode++;
                        currentlySnappedComponent = VisibleNodes[_currentlySnappedNode];
                        snapCursorToCurrentSnappedComponent();

                    } else if (CanScrollDown()) {
                        ScrollDown();
                    }
                    break;

                case 3:
                    SnapToCurrentListElement();
                    break;
            }
        }

        private void SnapToAcceptButton()
        {
            Game1.setMousePosition(AcceptButton.bounds.X + AcceptButton.bounds.Width / 2, AcceptButton.bounds.Y + AcceptButton.bounds.Height / 2);
        }

        private void SnapToCurrentListElement()
        {
            if (_currentlySnappedNode >= VisibleNodes.Count)
                _currentlySnappedNode = VisibleNodes.Count - 1;

            var node = VisibleNodes[_currentlySnappedNode];
            Game1.setMousePosition(node.bounds.X + node.bounds.Width / 2, node.bounds.Y + node.bounds.Height / 2);
        }

        private void NodeClicked(TarotListNode node)
        {
            if (node.Children.Any()) { //IsGroup
                node.IsExpanded = !node.IsExpanded;
                _expandedNodesCount += node.IsExpanded ? node.Children.Count : -node.Children.Count;

                AssignVisibleNodesTree();
                SetScrollBarToCurrentIndex();

                Game1.playSound("smallSelect");

            } else {
                CurrentModel = node.RoomModel;
                AssignPreview();

                Game1.playSound("coin");
            }
        }

        public override void draw(SpriteBatch b)
        {
            _mouseX = Game1.getMouseX();
            _mouseY = Game1.getMouseY();
            _hoveringButton = false;

            b.Draw(Game1.fadeToBlackRect, new Rectangle(0, 0, Game1.uiViewport.Width, Game1.uiViewport.Height), Color.Black * 0.5f); //BG Text
            b.Draw(BackgroundTexture, new Rectangle(xPositionOnScreen, yPositionOnScreen, width, height), new Rectangle(0, 0, 320, 180), Color.White); //BG Texture

            if (Preview is not null) {
                Rectangle previewRectangle = new Rectangle(xPositionOnScreen + 187 * 4, yPositionOnScreen + 36 * 4, 106 * 4, 74 * 4);
                b.Draw(Preview, previewRectangle, Color.White);
            }

            DrawTree(b);
            DrawButton(b, AcceptButton.bounds, "Accept", ButtonStyles.Accept);
            DrawScrollbar(b);
            if (upperRightCloseButton.bounds.Contains(_mouseX, _mouseY))
                _hoveringButton = true;

            upperRightCloseButton.draw(b);

            if (CurrentModel.Value?.DisplayName is not null)
                SpriteText.drawStringWithScrollCenteredAt(b, CurrentModel.Value.DisplayName, xPositionOnScreen + 241 * 4, yPositionOnScreen + 465);

            drawMouse(b, cursor: _hoveringButton && !_scrolling ? 44 : -1);
        }

        private void DrawScrollbar(SpriteBatch b)
        {
            if (!IsScrollbarDrawn())
                return;

            var scrollUpHoverOffset = _scrollUpButton.Contains(_mouseX, _mouseY) ? 8 : 0;
            var scrollDownHoverOffset = _scrollDownButton.Contains(_mouseX, _mouseY) ? 8 : 0;

            b.Draw(BackgroundTexture, _scrollUpButton, new Rectangle(192 + scrollUpHoverOffset, 192, 8, 7), Color.White);
            b.Draw(BackgroundTexture, _scrollbarBounds, new Rectangle(192, 199, 8, 165), Color.White);
            b.Draw(BackgroundTexture, _scrollDownButton, new Rectangle(192 + scrollDownHoverOffset, 364, 8, 7), Color.White);

            if (_scrollUpButton.Contains(_mouseX, _mouseY) || _scrollDownButton.Contains(_mouseX, _mouseY))
                _hoveringButton = true;

            _scrollbarRunner.draw(b);
        }

        private void DrawTree(SpriteBatch b)
        {
            foreach (var node in VisibleNodes) {
                DrawButton(b, node.bounds, node.name, node.Style);
            }
        }

        private void DrawButton(SpriteBatch b, Rectangle bounds, string text, ButtonStyles style)
        {
            var endPieceLength = 10 * 3;
            var middlePieceLength = bounds.Width - 2 * endPieceLength;
            bool isHovered = false;

            if (bounds.Contains(_mouseX, _mouseY)) {
                isHovered = true;
                _hoveringButton = true;
            }

            var color = Color.White * 0.75f;
            var leftRect = new Rectangle(bounds.X, bounds.Y, endPieceLength, bounds.Height);
            var middleRect = new Rectangle(leftRect.Right, leftRect.Y, middlePieceLength, leftRect.Height);
            var rightRect = new Rectangle(middleRect.Right, middleRect.Y, endPieceLength, middleRect.Height);

            var hoverOffset = isHovered ? 96 : 0;

            b.Draw(BackgroundTexture, leftRect, new Rectangle(0 + hoverOffset, 260, 10, 23), color);
            b.Draw(BackgroundTexture, middleRect, new Rectangle(10 + hoverOffset, 260, 76, 23), color);
            b.Draw(BackgroundTexture, rightRect, new Rectangle(86 + hoverOffset, 260, 10, 23), color);

            var textOffset = 0;
            if (style is ButtonStyles.Accept) {
                var diamondHoverOffset = isHovered ? 34 : 0;
                b.Draw(BackgroundTexture, new Rectangle(bounds.X + 24, bounds.Y + 18, 17 * 3, 13 * 3), new Rectangle(8, 201 + diamondHoverOffset, 17, 13), Color.White * 0.75f);
                b.Draw(BackgroundTexture, new Rectangle(bounds.Right - 24 - 17 * 3, bounds.Y + 18, 17 * 3, 13 * 3), new Rectangle(8, 201 + diamondHoverOffset, 17, 13), Color.White * 0.75f);

                textOffset = (int)((bounds.Width / 2f) - (SpriteText.getWidthOfString(text) / 2f)) - 32;

            } else if (style is ButtonStyles.ExpandedGroup or ButtonStyles.ClosedGroup) {

                var rotation = style is ButtonStyles.ClosedGroup ? 0 : 1.57f;
                Vector2 expandedOffset = style is ButtonStyles.ClosedGroup ? new() : new(16, 8);

                b.Draw(BackgroundTexture, new Vector2(bounds.X + 28 + expandedOffset.X, bounds.Y + 24 + expandedOffset.Y), new Rectangle(13, 298, 9, 13), color, rotation, new Vector2(), 2f, SpriteEffects.None, 1f);

                textOffset = 18;
            }

            var drawText = LimitTextWidth(text, bounds.Width - 30 - textOffset - 24);
            SpriteText.drawString(b, drawText, bounds.X + 32 - 2 + textOffset, bounds.Y + 16 - 2, color: Color.BlueViolet); //Shadow
            SpriteText.drawString(b, drawText, bounds.X + 32 + textOffset, bounds.Y + 16, color: isHovered ? Color.Cyan : Color.PowderBlue);
        }

        private static string LimitTextWidth(string text, int maxWidth)
        {
            var count = text.Count();
            if (count <= 10)
                return text;

            string result = text.Substring(0, 10);
            var dotWidth = SpriteText.getWidthOfString("..");

            for (int i = 10; i < count; i++) {
                if (SpriteText.getWidthOfString(result) > maxWidth - dotWidth)
                    return result + "..";

                result += text[i];
            }

            return text;
        }


        private bool CanScrollUp()
            => _scrollIndex > 0;

        private bool CanScrollDown()
            => _scrollIndex < _expandedNodesCount - _maxListElements;

        public bool IsScrollbarDrawn() => _totalNodesCount > _maxListElements;
    }
}
