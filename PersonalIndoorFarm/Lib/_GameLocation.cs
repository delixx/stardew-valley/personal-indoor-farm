﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Locations;
using StardewValley.Mods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class _GameLocation
    {
        public static void Initialize()
        {
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(GameLocation), nameof(GameLocation.GetSeason)),
                prefix: new HarmonyMethod(typeof(_GameLocation), nameof(_GameLocation.GetSeason_Prefix))
            );

            harmony.Patch( //this runs before Cabin.DeleteFarmhand
                original: AccessTools.DeclaredMethod(typeof(Cabin), nameof(Cabin.demolish)),
                prefix: new HarmonyMethod(typeof(_GameLocation), nameof(_GameLocation.demolish_Prefix))
            );
        }

        private static void demolish_Prefix(Cabin __instance)
        {
            if (Context.ScreenId > 0)
                return;

            if (!__instance.HasOwner)
                return;

            var sendData = new RemoveFarmhandModel(__instance.OwnerId, __instance.NameOrUniqueName);
            var farmer = Game1.GetPlayer(__instance.OwnerId);
            foreach (var doorId in Door.getDoorIds(farmer)) {
                if (!farmer.modData.TryGetValue(PersonalRoom.generateFarmerPIDKey(doorId), out var pid))
                    continue;

                var locationKey = PersonalRoom.generateLocationKey(pid, __instance.OwnerId, doorId);
                PersonalRoom.removeLocation(locationKey, __instance.NameOrUniqueName);
                sendData.PifLocations.Add(locationKey);
            }

            Helper.Multiplayer.SendMessage(sendData, "removeFarmHand", new[] { ModManifest.UniqueID });
        }

        public static bool GetSeason_Prefix(GameLocation __instance, ref Season __result)
        {
            if (__instance.NameOrUniqueName?.StartsWith(PersonalRoom.BaseLocationKey) != true)
                return true;

            if (!PersonalRoom.usesPifSeasons(__instance))
                return true;

            __result = PersonalRoom.getSeason(__instance);
            return false;
        }

    }
}
