﻿using Microsoft.Xna.Framework;
using StardewValley;
using StardewValley.GameData.Buildings;
using StardewValley.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class UtilityMisc
    {

        public static void placeInitialItems(GameLocation location, List<IndoorItemAdd> items)
        {
            foreach (IndoorItemAdd item in items) {
                Vector2 tileVector = Utility.PointToVector2(item.Tile);
                if (!location.IsTileBlockedBy(tileVector, CollisionMask.Furniture | CollisionMask.Objects) && ItemRegistry.Create(item.ItemId) is StardewValley.Object newObj) {
                    if (item.Indestructible) {
                        newObj.Fragility = 2;
                    }
                    newObj.TileLocation = tileVector;
                    newObj.Location = location;


                    if (newObj is Furniture newFurniture)
                        location.furniture.Add(newFurniture);

                    else
                        PlaceComplexObject(location, newObj);
                }
            }
        }

        //Imitating Object.placementAction
        private static void PlaceComplexObject(GameLocation location, StardewValley.Object obj)
        {
            var placementTile = obj.TileLocation;

            if (obj.HasContextTag("sign_item")) {
                location.objects.Add(obj.TileLocation, new Sign(obj.TileLocation, obj.ItemId) { Fragility = obj.Fragility });
                return;

            } else if (obj.HasContextTag("torch_item")) {
                new Torch(obj.ItemId, true) { Fragility = obj.Fragility }.placementAction(location, (int)obj.TileLocation.X, (int)obj.TileLocation.Y, Game1.player);
                return;
            }

            switch (obj.QualifiedItemId) {
                case "(BC)108":
                case "(BC)109": //Tub o Flowers
                    obj.ResetParentSheetIndex();
                    Season season = location.GetSeason();
                    if (location.IsOutdoors && (season == Season.Winter || season == Season.Fall)) {
                        obj.ParentSheetIndex = 109;
                    }
                    location.Objects.Add(placementTile, obj);
                    return;


                case "(BC)62":
                    location.Objects.Add(obj.TileLocation, new IndoorPot(placementTile) { Fragility = obj.Fragility });
                    return;

                case "(BC)71": //Staircase
                    return;

                case "(BC)256": //Junimo Chest
                case "(BC)248": //Mini-Shipping bin
                case "(BC)232":
                case "(BC)130":
                case "(BC)BigChest":
                case "(BC)BigStoneChest":
                    location.objects.Add(placementTile, new Chest(playerChest: true, placementTile, obj.ItemId) {
                        name = obj.name,
                        Fragility = obj.Fragility,
                    });
                    return;

                case "(BC)163":
                    location.objects.Add(placementTile, new Cask(placementTile));
                    return;

                case "(BC)165": //Autograbber
                    location.objects.Add(placementTile, obj);
                    obj.heldObject.Value = new Chest();
                    return;

                case "(BC)208":
                    location.objects.Add(placementTile, new Workbench(placementTile) { Fragility = obj.Fragility });
                    return;

                case "(BC)209":
                    location.objects.Add(placementTile, new MiniJukebox(placementTile) { Fragility = obj.Fragility });
                    return;

                case "(BC)211":
                    location.objects.Add(placementTile, new WoodChipper(placementTile) { Fragility = obj.Fragility });
                    return;

                case "(BC)214":
                    location.objects.Add(placementTile, new Phone(placementTile) { Fragility = obj.Fragility });
                    return;


                case "(BC)216":
                    Chest fridge = new Chest("216", placementTile, 217, 2) {
                        Fragility = obj.Fragility
                    };
                    fridge.fridge.Value = true;
                    location.objects.Add(placementTile, fridge);
                    return;

                case "(BC)238": //Mini-Obelisk
                    return;

                //case "(BC)254": Ostrich Incubator

                case "(BC)275": //Hopper
                    Chest hopper = new Chest(playerChest: true, placementTile, obj.ItemId) {
                        name = obj.name,
                        shakeTimer = 50,
                        Fragility = obj.Fragility,
                    };
                    hopper.lidFrameCount.Value = 2;
                    location.objects.Add(placementTile, hopper);
                    return;

                default:
                    location.objects.Add(placementTile, obj);
                    return;
            }
        }

        /// <summary>
        /// Perform an action for each PIF room in the game.
        /// 1. Room GameLocation
        /// 2. Owner Farmer
        /// 3. PID string
        /// 4. Model PersonalRoomModel
        /// (out) shouldContinue bool
        /// </summary>
        /// <param name=""></param>
        public static void forEachRoom(Func<GameLocation, Farmer, PersonalRoomModel, string, bool> action)
        {
            foreach (var farmer in Game1.getAllFarmers()) {
                foreach (var doorId in Door.getDoorIds(farmer)) {
                    if (!farmer.modData.TryGetValue(PersonalRoom.generateFarmerPIDKey(doorId), out var pid))
                        continue;

                    var loc = Game1.getLocationFromName(PersonalRoom.generateLocationKey(pid, farmer.UniqueMultiplayerID, doorId));

                    if (loc is null) {
                        Monitor.LogOnce("Location does not exist: " + PersonalRoom.generateLocationKey(pid, farmer.UniqueMultiplayerID, doorId));
                        continue;
                    }

                    var model = PersonalRoom.getModel(pid);

                    if (!action(loc, farmer, model, pid))
                        return;
                }
            }
        }
    }
}
