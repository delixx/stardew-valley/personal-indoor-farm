﻿using Microsoft.Xna.Framework;
using StardewValley.GameData.Buildings;
using StardewValley.GameData.Locations;
using StardewValley.GameData.Minecarts;
using System.Collections.Generic;

namespace PersonalIndoorFarm.Lib
{
    public class PersonalRoomModel : LocationData
    {
        public string MapAsset_T0;
        public string MapAsset_T1;
        public string MapAsset_T2;
        public string MapAsset_T3;

        public Point ArrivalTile_T0;
        public Point ArrivalTile_T1;
        public Point ArrivalTile_T2;
        public Point ArrivalTile_T3;

        public int? ArrivalDirection_T0 = null;
        public int? ArrivalDirection_T1 = null;
        public int? ArrivalDirection_T2 = null;
        public int? ArrivalDirection_T3 = null;

        public string Preview;
        public string Group;

        public bool UseVanillaSeasonLogic;
        public bool ExcludeFromRoomSelection;

        //Not quite sure how I'll make it for other tiers, or even if.
        public List<IndoorItemAdd> InitialItems_T0 = new();

        public Dictionary<string, MinecartDestinationData> MinecartDestinations = new();
    }
}
