﻿using StardewModdingAPI;
using StardewValley;
using StardewValley.Delegates;
using StardewValley.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class SpaceTimeSynchronizer
    {
        public const string ItemId = "DLX.PIF_Synchronizer";
        public const string QualifiedItemId = "(O)" + ItemId;
        public const string BuffId = "DLX.PIF_SpaceTimeBuff";
        public const string BuffTriggerAction = BuffId + "Trigger";
        public static void Initialize()
        {
            TriggerActionManager.RegisterAction(BuffTriggerAction, ProcessAction);
        }

        private static bool ProcessAction(string[] args, TriggerActionContext context, out string error)
        {
            if(!ArgUtility.TryGet(args, 1, out var buffId, out error))

            error = null;

            var location = Game1.currentLocation;
            if (location.NameOrUniqueName?.StartsWith(PersonalRoom.BaseLocationKey) == false)
                return true;

            if (!PersonalRoom.isOwner(location, Game1.player))
                return true;

            PersonalRoom.setInitialDayAndSeason(location);
            return true;
        }
    }
}
