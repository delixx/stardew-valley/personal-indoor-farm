﻿using PersonalIndoorFarm.Lib.Warping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    public class Main
    {

        public static void Initialize()
        {
            AssetRequested.Initialize();
            ModDataEvents.Initialize();

            Door.Initialize();
            Painting.Initialize();
            PersonalRoom.Initialize();
            _GameLocation.Initialize();
            DayUpdate.Initialize();
            VoidSeal.Initialize();
            SpaceTimeSynchronizer.Initialize();
            MapProperties.Initialize();
            MinecartDestinations.Initialize();

            WarpToFarmHouse.Initialize();
        }
    }
}
