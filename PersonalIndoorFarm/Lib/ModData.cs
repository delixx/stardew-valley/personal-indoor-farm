﻿using PersonalIndoorFarm.Lib.Warping;
using StardewModdingAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class ModData
    {
        private static ModData _instance = null;
        private const string SaveKey = "main";

        /// <summary>
        /// Remembers the last used PIF door in case the room was entered through unusual means like a minecart
        /// </summary>
        public Dictionary<long, Dictionary<string, Entrance>> LastEntranceDictionary = new();

        /// <summary>
        /// A Stack that recursively remembers what PIF room one entered
        /// </summary>
        public Dictionary<long, OrderedStack<Entrance>> LastEntranceStack = new();

        public static ModData GetInstance(bool reload = false)
        {
            if(_instance is not null && !reload)
                return _instance;

            if (!Context.IsMainPlayer)
                return new();

            return _instance = Helper.Data.ReadSaveData<ModData>(SaveKey) ?? new ModData();
        }

        public static void SetInstance(ModData modData) => _instance = modData;

        public static void DiscardInstance() => _instance = null;

        public static void SaveInstance() => Helper.Data.WriteSaveData(SaveKey, _instance);

        public Dictionary<string, Entrance> LastEntranceDictionaryForPlayer(long id)
        {
            if (!LastEntranceDictionary.TryGetValue(id, out var entranceDictionary))
                entranceDictionary = LastEntranceDictionary[id] = new();

            return entranceDictionary;
        }

        public OrderedStack<Entrance> LastEntranceStackForPlayer(long id)
        {
            if (!LastEntranceStack.TryGetValue(id, out var entranceStack))
                entranceStack = LastEntranceStack[id] = new();

            return entranceStack;
        }
    }
}
