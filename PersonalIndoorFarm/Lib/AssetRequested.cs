﻿using Microsoft.Xna.Framework.Graphics;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using StardewValley.GameData.Buffs;
using StardewValley.GameData.Locations;
using StardewValley.GameData.Minecarts;
using StardewValley.GameData.Objects;
using StardewValley.GameData.Shops;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xTile;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    public class AssetRequested
    {
        public const string FarmsAsset = "DLX.PIF/Farms";
        public const string DoorsAsset = "DLX.PIF/Doors";
        public const string SpriteSheetAsset = "DLX.PIF_SpriteSheet";
        public const string ShopId = "DLX.PIF.Magician";
        private static Texture2D _SpriteSheetTexture;
        public static Texture2D SpriteSheetTexture { get => _SpriteSheetTexture ??= Helper.GameContent.Load<Texture2D>(SpriteSheetAsset); set => _SpriteSheetTexture = value; }
        public static void Initialize()
        {
            Helper.Events.Content.AssetRequested += OnAssetRequested;
        }

        private static void OnAssetRequested(object sender, AssetRequestedEventArgs e)
        {
            if (e.NameWithoutLocale.IsEquivalentTo(FarmsAsset)) {
                //We need to provide an empty initial dictionary where others can append their changes
                e.LoadFrom(delegate () {
                    return new Dictionary<string, PersonalRoomModel>() { };
                }, AssetLoadPriority.Exclusive);

            } else if (e.NameWithoutLocale.IsEquivalentTo(DoorsAsset)) {
                e.LoadFrom(delegate () {
                    return new Dictionary<string, DoorAssetModel>() {
                        { $"{Door.ItemId}_Aether",  new(doorId: "Aether")},
                        { $"{Door.ItemId}_Erebus",  new(doorId: "Erebus") },
                        { $"{Door.ItemId}_Chaos",   new(doorId:"Chaos") },

                        { $"{Door.ItemId}_WoodStep_Attic",  new(Door.WoodStepSound, "WoodStep_Attic") },
                        { $"{Door.ItemId}_WoodStep_Loft",   new(Door.WoodStepSound, "WoodStep_Loft") },
                        { $"{Door.ItemId}_WoodStep_Cellar", new(Door.WoodStepSound, "WoodStep_Cellar") },

                        { $"{Door.ItemId}_Spa", new("doorOpen", "Spa") },

                        //Vanilla
                        { "DecorativeJojaDoor",     new(doorId: "Vanilla.DecorativeJojaDoor") },
                        { "DecorativeWizardDoor",   new(doorId: "Vanilla.DecorativeWizardDoor") },
                        { "DecorativeJunimoDoor",   new(doorId: "Vanilla.DecorativeJunimoDoor") },
                        { "DecorativeRetroDoor",    new(doorId: "Vanilla.DecorativeRetroDoor") },
                        { "DecorativeDoor1",        new(doorId: "Vanilla.DecorativeDoor1") },
                        { "DecorativeDoor2",        new(doorId: "Vanilla.DecorativeDoor2") },
                        { "DecorativeDoor3",        new(doorId: "Vanilla.DecorativeDoor3") },
                        { "DecorativeDoor4",        new(doorId: "Vanilla.DecorativeDoor4") },
                        { "DecorativeDoor5",        new(doorId: "Vanilla.DecorativeDoor5") },
                        { "DecorativeDoor6",        new(doorId: "Vanilla.DecorativeDoor6") },
                        { "DecorativeOakLadder",    new(Door.WoodStepSound, "Vanilla.DecorativeOakLadder") },
                        { "DecorativeWalnutLadder", new(Door.WoodStepSound, "Vanilla.DecorativeWalnutLadder") },
                        { "DecorativeHatch",        new("doorCreak", "Vanilla.DecorativeHatch") },

                        //VMV
                        { "Lumisteria.MtVapius_FurnitureDeluxeSet_Door01", new() },
                        { "Lumisteria.MtVapius_FurnitureDarkDeluxeSet_Door01", new() },

                        //Seasonal French Doors
                        { "SeasonalFrD_2c", new(doorId: "OB7.FWindows_2") },
                        { "SeasonalFrD_2h", new(doorId: "OB7.FWindows_2") },
                        { "SeasonalFrD_2o", new(doorId: "OB7.FWindows_2") },
                        { "SeasonalFrD_3c", new(doorId: "OB7.FWindows_3") },
                        { "SeasonalFrD_3h", new(doorId: "OB7.FWindows_3") },
                        { "SeasonalFrD_3o", new(doorId: "OB7.FWindows_3") },
                        { "SeasonalFrD_2c_curtain", new(doorId: "OB7.FWindows_2_curtain") },
                        { "SeasonalFrD_2h_curtain", new(doorId: "OB7.FWindows_2_curtain") },
                        { "SeasonalFrD_2o_curtain", new(doorId: "OB7.FWindows_2_curtain") },
                        { "SeasonalFrD_3c_curtain", new(doorId: "OB7.FWindows_3_curtain") },
                        { "SeasonalFrD_3h_curtain", new(doorId: "OB7.FWindows_3_curtain") },
                        { "SeasonalFrD_3o_curtain", new(doorId: "OB7.FWindows_3_curtain") },

                        //DTANaDaWTNaRW
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_black", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_wooden", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_dark", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_dark wooden", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_red", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_rasmodius", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.gothdoor1_blue", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.sundoor2_black", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.sundoor2_wooden", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.sundoor2_dark wooden", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.sundoor2_rasmodius", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.sundoor2_blue", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.sundoor2_red", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.heartdoor3_black", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.heartdoor3_wooden", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.heartdoor3_dark wooden", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.heartdoor3_rasmodius", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.heartdoor3_blue", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.heartdoor3_red", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.fairydoor4_pale", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.fairydoor4_black", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.fairydoor4_rasmodius", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.fairydoor4_red", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.fairydoor4_blue", new() },
                        {"spacesheepcaptain.DTANaDaWTNaRW.fairydoor4_green", new() },
                    };
                }, AssetLoadPriority.High);

            } else if (e.NameWithoutLocale.IsEquivalentTo(SpriteSheetAsset)) {
                e.LoadFrom(delegate () {
                    return Helper.ModContent.Load<Texture2D>("assets/SpriteSheet.png");
                }, AssetLoadPriority.Medium);

            } else if (e.NameWithoutLocale.IsEquivalentTo("Data/Furniture")) {
                e.Edit(asset => {
                    var editor = asset.AsDictionary<string, string>();
                    var data = editor.Data;

                    addDoor(data, "Aether", 1000, "Door1.Name", 0);
                    addDoor(data, "Erebus", 15000, "Door2.Name", 6);
                    addDoor(data, "Chaos", 20000, "Door3.Name", 7);

                    addDoor(data, "WoodStep_Attic", 20000, "DoorAttic.Name", 30, 2);
                    addDoor(data, "WoodStep_Loft", 20000, "DoorLoft.Name", 32, 2);
                    addDoor(data, "WoodStep_Cellar", 20000, "DoorCellar.Name", 34, 2);

                    addDoor(data, "Spa", 20000, "DoorSpa.Name", 36);

                    data.Add(Painting.ItemId,
                        "Painting of the Season" + //name
                        "/painting" + //type
                        "/1 2" + //tilesheet size
                        "/1 2" + //bounding box size
                        "/1" + //rotations
                        "/500" + //price
                        "/-1" + //placement restriction
                        $"/[LocalizedText Strings\\Furniture:DLX.PIF.Painting.Name]" + //display name
                        "/1" + //Index in sprite sheet
                        $"/{SpriteSheetAsset}"
                    );
                });

            } else if (e.NameWithoutLocale.IsEquivalentTo("Strings/Furniture")) {
                e.Edit(asset => {
                    var editor = asset.AsDictionary<string, string>();
                    var data = editor.Data;

                    data.Add("DLX.PIF.Door1.Name", Helper.Translation.Get("Door1.Name"));
                    data.Add("DLX.PIF.Door2.Name", Helper.Translation.Get("Door2.Name"));
                    data.Add("DLX.PIF.Door3.Name", Helper.Translation.Get("Door3.Name"));

                    data.Add("DLX.PIF.DoorAttic.Name", Helper.Translation.Get("DoorAttic.Name"));
                    data.Add("DLX.PIF.DoorLoft.Name", Helper.Translation.Get("DoorLoft.Name"));
                    data.Add("DLX.PIF.DoorCellar.Name", Helper.Translation.Get("DoorCellar.Name"));

                    data.Add("DLX.PIF.DoorSpa.Name", Helper.Translation.Get("DoorSpa.Name"));

                    data.Add("DLX.PIF.Painting.Name", Helper.Translation.Get("Painting.Name"));
                });

            } else if (e.NameWithoutLocale.IsEquivalentTo("Data/Objects")) {
                e.Edit(asset => {
                    var editor = asset.AsDictionary<string, ObjectData>();

                    editor.Data.Add(SpaceTimeSynchronizer.ItemId, new ObjectData() {
                        Name = "DLX.PIF.SpaceTimeSynchronizer",
                        DisplayName = Helper.Translation.Get("Synchronizer.Name"),
                        Description = Helper.Translation.Get("Synchronizer.Description"),
                        Type = "Basic",
                        Category = StardewValley.Object.artisanGoodsCategory,
                        Price = 100,
                        Texture = SpriteSheetAsset,
                        SpriteIndex = 5,
                        Edibility = -4,
                        IsDrink = true,
                        Buffs = new List<ObjectBuffData>() { new ObjectBuffData() {
                            BuffId = SpaceTimeSynchronizer.BuffId,
                            Duration = 30
                        } },
                        ExcludeFromRandomSale = true,
                        ExcludeFromShippingCollection = true,
                        ExcludeFromFishingCollection = true,
                    });

                    editor.Data.Add(VoidSeal.ItemId, new ObjectData() {
                        Name = "DLX.PIF.VoidSealLiquid",
                        DisplayName = Helper.Translation.Get("VoidSeal.Name"),
                        Description = Helper.Translation.Get("VoidSeal.Description"),
                        Type = "Basic",
                        Category = StardewValley.Object.artisanGoodsCategory,
                        Price = 250,
                        Texture = SpriteSheetAsset,
                        SpriteIndex = 15,
                        Edibility = -10,
                        IsDrink = true,
                        Buffs = new List<ObjectBuffData>() { new ObjectBuffData() {
                            BuffId = VoidSeal.BuffId,
                            Duration = 30
                        } },
                        ExcludeFromRandomSale = true,
                        ExcludeFromShippingCollection = true,
                        ExcludeFromFishingCollection = true,
                    });

                    editor.Data.Add(Key.ItemId, new ObjectData() {
                        Name = "DLX.PIF.Key",
                        DisplayName = Helper.Translation.Get("Key.Name"),
                        Description = Helper.Translation.Get("Key.Description"),
                        Type = "Basic",
                        Category = 0,
                        Price = 1250,
                        Texture = SpriteSheetAsset,
                        SpriteIndex = 25,
                        ExcludeFromRandomSale = true,
                        ExcludeFromShippingCollection = true,
                        ExcludeFromFishingCollection = true,
                    });
                });

            } else if (e.NameWithoutLocale.IsEquivalentTo("Data/Buffs")) {
                e.Edit(asset => {
                    var editor = asset.AsDictionary<string, BuffData>();

                    editor.Data.Add(SpaceTimeSynchronizer.BuffId, new BuffData {
                        Duration = 30,
                        GlowColor = "Purple",
                        DisplayName = Helper.Translation.Get("Synchronizer.Buff.Name"),
                        Description = Helper.Translation.Get("Synchronizer.Buff.Description"),
                        IconSpriteIndex = 14,
                        IconTexture = "TileSheets\\BuffsIcons",
                        ActionsOnApply = new() { SpaceTimeSynchronizer.BuffTriggerAction }
                    });

                    editor.Data.Add(VoidSeal.BuffId, new BuffData {
                        Duration = 30,
                        GlowColor = "MediumPurple",
                        DisplayName = Helper.Translation.Get("VoidSeal.Buff.Name"),
                        Description = Helper.Translation.Get("VoidSeal.Buff.Description"),
                        IconSpriteIndex = 14,
                        IconTexture = "TileSheets\\BuffsIcons",
                        ActionsOnApply = new() { VoidSeal.BuffTriggerAction }
                    });
                });

            } else if (e.NameWithoutLocale.IsEquivalentTo("Data/Shops")) {
                e.Edit(asset => {
                    var editor = asset.AsDictionary<string, ShopData>();
                    editor.Data.Add(ShopId, new() {
                        Items = new() {
                        new() { Id = SpaceTimeSynchronizer.QualifiedItemId, ItemId = SpaceTimeSynchronizer.QualifiedItemId },
                        new() { Id = VoidSeal.QualifiedItemId, ItemId = VoidSeal.QualifiedItemId },
                        new() { Id = Painting.QualifiedItemId, ItemId = Painting.QualifiedItemId },
                        new() { Id = Key.QualifiedItemId, ItemId = Key.QualifiedItemId },

                        new() { Id = $"{Door.QualifiedItemId}_Aether", ItemId = $"{Door.QualifiedItemId}_Aether" },
                        new() { Id = $"{Door.QualifiedItemId}_Erebus", ItemId = $"{Door.QualifiedItemId}_Erebus" },
                        new() { Id = $"{Door.QualifiedItemId}_Chaos", ItemId = $"{Door.QualifiedItemId}_Chaos" },
                        new() { Id = $"{Door.QualifiedItemId}_Attic", ItemId = $"{Door.QualifiedItemId}_WoodStep_Attic" },
                        new() { Id = $"{Door.QualifiedItemId}_Loft", ItemId = $"{Door.QualifiedItemId}_WoodStep_Loft" },
                        new() { Id = $"{Door.QualifiedItemId}_Cellar", ItemId = $"{Door.QualifiedItemId}_WoodStep_Cellar" },
                        new() { Id = $"{Door.QualifiedItemId}_Spa", ItemId = $"{Door.QualifiedItemId}_Spa" },
                    }
                    });
                });

            } else if (e.NameWithoutLocale.IsEquivalentTo("Maps/Tunnel")) {
                e.Edit(asset => {
                    var editor = asset.AsMap();
                    var patch = Helper.ModContent.Load<Map>("assets/Shopkeeper.tmx");
                    editor.PatchMap(patch, null, targetArea: new Microsoft.Xna.Framework.Rectangle(10, 6, 1, 2));
                });

            } else if (e.NameWithoutLocale.IsEquivalentTo(@"Data\Minecarts")) {
                MinecartDestinations.appendMinecartDestinations(e);

            }
        }

        public static void addDoor(IDictionary<string, string> data, string id, int price, string translation, int index, int w = 1, int h = 3)
        {
            data.Add($"{Door.ItemId}_{id}",
                $"Decorative Door" + //name
                "/painting" + //type
                $"/{w} {h}" + //tilesheet size
                $"/{w} {h}" + //bounding box size
                "/1" + //rotations
                $"/{price}" + //price
                "/-1" + //placement restriction
                $"/[LocalizedText Strings\\Furniture:DLX.PIF.{translation}]" + //display name
                $"/{index}" + //Index in sprite sheet
                $"/{SpriteSheetAsset}"
            );
        }
    }
}
