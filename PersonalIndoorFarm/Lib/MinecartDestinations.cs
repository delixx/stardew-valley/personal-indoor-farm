﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using PersonalIndoorFarm.Lib.Warping;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using StardewValley.Delegates;
using StardewValley.GameData.Minecarts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class MinecartDestinations
    {
        const string CanAccessRoomGSQ = "DLX.PIF_CanAccessRoom";

        public static void Initialize()
        {
            GameStateQuery.Register(CanAccessRoomGSQ, handleCanAccessRoomGSQ);
            GameLocation.RegisterTileAction("DLX.PIF_MinecartTransport", handleMinecartTransport);

            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(GameLocation), nameof(GameLocation.MinecartWarp)),
                prefix: new HarmonyMethod(typeof(MinecartDestinations), nameof(MinecartDestinations.MinecartWarp_Prefix))
            );
        }

        private static void MinecartWarp_Prefix(MinecartDestinationData destination)
        {
            if (destination.TargetLocation?.StartsWith(PersonalRoom.BaseLocationKey) == true)
                WarpToFarmHouse.PushLastEntrance(true, destination.TargetLocation);

            else if (Game1.currentLocation?.Name?.StartsWith(PersonalRoom.BaseLocationKey) == true)
                if (WarpToFarmHouse.LastEntranceStack.TryPop(out _))
                    WarpToFarmHouse.LastEntranceChanged();
        }

        private static bool handleMinecartTransport(GameLocation location, string[] args, Farmer player, Point tile)
        {
            string networkId = ArgUtility.Get(args, 1) ?? "Default";
            string excludeDestinationId = location.Name;
            location.ShowMineCartMenu(networkId, excludeDestinationId);

            return true;
        }

        private static bool handleCanAccessRoomGSQ(string[] query, GameStateQueryContext context)
        {
            if (!ArgUtility.TryGet(query, 1, out var roomName, out var error)) {
                return GameStateQuery.Helpers.ErrorResult(query, error);
            }

            var location = Game1.getLocationFromName(roomName);

            if (!long.TryParse(location.modData[PersonalRoom.OwnerKey], out var ownerId)) {
                Monitor.LogOnce($"OwnerKey did not parse to long in {location.Name}: {location.modData[PersonalRoom.OwnerKey]}");
                return false;
            }

            var owner = Game1.GetPlayer(ownerId);
            var doorId = location.modData[PersonalRoom.DoorIdKey];

            var isOwner = ownerId == Game1.player.UniqueMultiplayerID;

            var lockStatus = Key.getDoorLocked(owner, doorId);
            if (!isOwner && lockStatus == DoorLockEnum.Locked)
                return false;

            if (!isOwner && lockStatus == DoorLockEnum.LockedWhenOffline && !Game1.getOnlineFarmers().Any(e => e.UniqueMultiplayerID == owner.UniqueMultiplayerID))
                return false;

            return true;
        }

        public static void appendMinecartDestinations(AssetRequestedEventArgs e)
        {
            e.Edit(asset => {
                var data = asset.GetData<Dictionary<string, MinecartNetworkData>>();

                UtilityMisc.forEachRoom(delegate (GameLocation room, Farmer owner, PersonalRoomModel model, string pid) {

                    foreach (var destination in model.MinecartDestinations) {
                        if (!data.TryGetValue(destination.Key, out var network)) {
                            Monitor.LogOnce($"Invalid Minecart Network ID in PIF '{pid}' MinecartDestination: {destination.Key}", LogLevel.Warn);
                            continue;
                        }

                        var newDestination = new MinecartDestinationData();

                        newDestination.Id = room.Name;
                        newDestination.TargetLocation = room.Name;

                        var condition = $"{CanAccessRoomGSQ} \"{room.Name}\"";
                        newDestination.Condition = string.IsNullOrEmpty(destination.Value.Condition)
                            ? condition
                            : destination.Value.Condition + ", " + condition;

                        newDestination.DisplayName = string.IsNullOrEmpty(destination.Value.DisplayName)
                            ? $"{model.DisplayName} ({owner.displayName})"
                            : destination.Value.DisplayName;

                        newDestination.BuyTicketMessage = destination.Value.BuyTicketMessage;
                        newDestination.Price = destination.Value.Price;
                        newDestination.TargetDirection = destination.Value.TargetDirection;
                        newDestination.TargetTile = destination.Value.TargetTile;
                        newDestination.CustomFields = destination.Value.CustomFields;

                        network.Destinations.Add(newDestination);
                        Monitor.LogOnce($"Added new minecart destination {newDestination.TargetLocation} to network {destination.Key}");
                    }

                    return true;
                });
            }, AssetEditPriority.Late);
        }

    }
}
