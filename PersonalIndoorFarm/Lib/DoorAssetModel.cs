﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    public class DoorAssetModel
    {
        public string Sound = "doorOpen";
        public string DoorId = "";
        public string RoomId = "";

        public DoorAssetModel(string sound = "doorOpen", string doorId = "", string roomId = "")
        {
            Sound = sound;
            DoorId = doorId;
            RoomId = roomId;
        }
    }
}
