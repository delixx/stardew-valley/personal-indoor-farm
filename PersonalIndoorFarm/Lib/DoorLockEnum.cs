﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    public enum DoorLockEnum
    {
        Unlocked,
        Locked,
        LockedWhenOffline
    }
}
