﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    /// <summary>
    /// Makeshift Stack that doesn't reverse order when saving / loading
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OrderedStack<T> : List<T>
    {
        public void Push(T item) => Add(item);

        public T Pop()
        {
            if (Count == 0)
                throw new InvalidOperationException("Stack is empty.");
            T item = this[^1];
            RemoveAt(Count - 1);
            return item;
        }

        public bool TryPop([MaybeNullWhen(false)] out T result)
        {
            result = default;

            if (Count == 0)
                return false;

            result = Pop();
            return true;
        }

        public T Peek()
        {
            if (Count == 0)
                throw new InvalidOperationException("Stack is empty.");
            return this[^1];
        }
    }
}
