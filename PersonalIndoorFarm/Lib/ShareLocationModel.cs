﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    internal class ShareLocationModel
    {
        public string Pid;
        public string DoorId;
        public long PlayerId;

        public ShareLocationModel(string pid, string doorId, long playerId)
        {
            Pid = pid;
            DoorId = doorId;
            PlayerId = playerId;
        }
    }
}
