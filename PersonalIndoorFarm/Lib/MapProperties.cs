﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using StardewValley;
using StardewValley.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class MapProperties
    {
        public const string EnableFridgePlacement = "DLX.PIF_EnableFridgePlacement";

        public static void Initialize()
        {
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(StardewValley.Object), nameof(StardewValley.Object.placementAction)),
                prefix: new HarmonyMethod(typeof(MapProperties), nameof(MapProperties.placementAction_Prefix))
            );
        }

        private static bool placementAction_Prefix(StardewValley.Object __instance, ref bool __result, GameLocation location, int x, int y, Farmer who = null)
        {
            if (__instance.QualifiedItemId == "(BC)216") {
                if (!location.TryGetMapProperty(EnableFridgePlacement, out _))
                    return true;

                Vector2 placementTile = new Vector2(x / 64, y / 64);
                __instance.Location = location;
                __instance.TileLocation = placementTile;
                __instance.owner.Value = who?.UniqueMultiplayerID ?? Game1.player.UniqueMultiplayerID;

                Chest fridge = new Chest("216", placementTile, 217, 2);
                fridge.fridge.Value = true;

                location.objects.Add(placementTile, fridge);
                location.playSound("hammer");
                __result = true;
                return false;
            }

            return true;
        }
    }
}
