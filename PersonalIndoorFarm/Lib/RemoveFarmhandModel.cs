﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    internal class RemoveFarmhandModel
    {
        public List<string> PifLocations = new();
        public long Who;
        public string CabinLocation;
        public RemoveFarmhandModel(long who, string cabinLocation)
        {
            Who = who;
            CabinLocation = cabinLocation;
        }
    }
}
