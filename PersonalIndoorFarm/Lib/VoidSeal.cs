﻿using StardewModdingAPI;
using StardewValley;
using StardewValley.Delegates;
using StardewValley.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class VoidSeal
    {
        public const string ItemId = "DLX.PIF_VoidSeal";
        public const string QualifiedItemId = "(O)" + ItemId;
        public const string BuffId = "DLX.PIF_VoidSealBuff";
        public const string BuffTriggerAction = BuffId + "Trigger";
        public static void Initialize()
        {
            TriggerActionManager.RegisterAction(BuffTriggerAction, ProcessAction);
        }

        private static bool ProcessAction(string[] args, TriggerActionContext context, out string error)
        {
            error = null;

            var location = Game1.currentLocation;
            if (location.NameOrUniqueName?.StartsWith(PersonalRoom.BaseLocationKey) == false)
                return true;

            if (!PersonalRoom.isOwner(location, Game1.player))
                return false;

            var doorModData = PersonalRoom.getDoorModData(location, Game1.player);

            Helper.Multiplayer.SendMessage(location.NameOrUniqueName, "sealLocation", new[] { ModInstance.ModManifest.UniqueID });

            PersonalRoom.removeLocation(location.Name);
            Game1.player.modData.Remove(doorModData.Key);

            return true;
        }

        public static void SealRoom(GameLocation location, Farmer owner)
        {

        }
    }
}
