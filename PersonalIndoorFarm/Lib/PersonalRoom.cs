﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using PersonalIndoorFarm.Lib.Warping;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using StardewValley;
using StardewValley.GameData.Buildings;
using StardewValley.GameData.Locations;
using StardewValley.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    public class PersonalRoom
    {
        public const string BaseFarmerPidKey = "DLX.PIF_PersonalFarmID";
        private const string DayOfMonthKey = "DLX.PIF_DayOfMonth";
        private const string SeasonKey = "DLX.PIF_Season";
        public const string OwnerKey = "DLX.PIF_Owner";
        public const string DoorIdKey = "DLX.PIF_DoorId";
        public const string PIDKey = "DLX.PIF_PID";

        //Mine because the game needs that hard coded string to play footsteps properly :)
        public const string BaseLocationKey = "DLX.PIF_MINE";
        public static void Initialize()
        {
            Helper.Events.Multiplayer.ModMessageReceived += ModMessageReceived;
        }

        private static void ModMessageReceived(object sender, StardewModdingAPI.Events.ModMessageReceivedEventArgs e)
        {
            if (e.Type == "shareLocation") {
                var data = e.ReadAs<ShareLocationModel>();
                var farmer = Game1.GetPlayer(data.PlayerId);
                createLocation(data.Pid, farmer, data.DoorId);
                Helper.GameContent.InvalidateCache(@"Data\Minecarts");

            } else if (e.Type == "removeFarmHand") {
                var data = e.ReadAs<RemoveFarmhandModel>();
                foreach (var location in data.PifLocations)
                    removeLocation(location, data.CabinLocation);
                Helper.GameContent.InvalidateCache(@"Data\Minecarts");

            } else if (e.Type == "sealLocation") {
                var name = e.ReadAs<string>();
                removeLocation(name);
                Helper.GameContent.InvalidateCache(@"Data\Minecarts");

            }
        }

        public static CreateLocationData getCreateLocationData(string pid, Farmer who)
        {
            var model = getModel(pid);

            if (model.CreateOnLoad is null)
                model.CreateOnLoad = new CreateLocationData();

            model.CreateOnLoad.MapPath = getMapAsset(who, model);
            model.CreateOnLoad.Type ??= "StardewValley.Locations.DecoratableLocation";

            return model.CreateOnLoad;
        }

        public static PersonalRoomModel getModel(string pid)
        {
            var asset = Helper.GameContent.Load<Dictionary<string, PersonalRoomModel>>(AssetRequested.FarmsAsset);

            if (!asset.TryGetValue(pid, out var model)) {
                Monitor.LogOnce($"Invalid PIF Room ID: {pid}", LogLevel.Error);
                throw new MissingRoomException(pid);
            }

            return model;
        }

        public static string getMapAsset(Farmer who, PersonalRoomModel model)
        {
            if (who.HouseUpgradeLevel >= 3 && !string.IsNullOrEmpty(model.MapAsset_T3))
                return model.MapAsset_T3;

            if (who.HouseUpgradeLevel >= 2 && !string.IsNullOrEmpty(model.MapAsset_T2))
                return model.MapAsset_T2;

            if (who.HouseUpgradeLevel >= 1 && !string.IsNullOrEmpty(model.MapAsset_T1))
                return model.MapAsset_T1;

            return model.MapAsset_T0;
        }

        public static Point getArrivalTile(Farmer who, PersonalRoomModel model)
        {
            if (who.HouseUpgradeLevel >= 3 && !string.IsNullOrEmpty(model.MapAsset_T3))
                return model.ArrivalTile_T3;

            if (who.HouseUpgradeLevel >= 2 && !string.IsNullOrEmpty(model.MapAsset_T2))
                return model.ArrivalTile_T2;

            if (who.HouseUpgradeLevel >= 1 && !string.IsNullOrEmpty(model.MapAsset_T1))
                return model.ArrivalTile_T1;

            return model.ArrivalTile_T0;
        }

        public static int? getArrivalDirection(Farmer who, PersonalRoomModel model)
        {
            if (who.HouseUpgradeLevel >= 3 && model.ArrivalDirection_T3 is not null)
                return model.ArrivalDirection_T3;

            if (who.HouseUpgradeLevel >= 2 && model.ArrivalDirection_T2 is not null)
                return model.ArrivalDirection_T2;

            if (who.HouseUpgradeLevel >= 1 && model.ArrivalDirection_T1 is not null)
                return model.ArrivalDirection_T1;

            return model.ArrivalDirection_T0;
        }

        public static void placeInitialItems(GameLocation location, string pid)
        {
            var model = getModel(pid);
            UtilityMisc.placeInitialItems(location, model.InitialItems_T0);
        }

        public static void setInitialDayAndSeason(GameLocation location)
        {
            setDay(location, Game1.dayOfMonth.ToString());
            setSeason(location, Game1.season.ToString());
        }

        public static void setDay(GameLocation location, string day)
            => location.modData[DayOfMonthKey] = day;

        public static void setSeason(GameLocation location, string season)
            => location.modData[SeasonKey] = season;

        public static bool usesPifSeasons(GameLocation location)
        {
            if (!location.modData.TryGetValue(PIDKey, out var pid))
                pid = getPidFromName(location.NameOrUniqueName);

            //Splitscreen jank where Game1.IsMasterGame can be true, but Context.ScreenId > 0
            //No other farmers except the current splitscreen player are loaded
            //We are skipping this since it gets reevaluated later anyhow
            if (pid is null)
                return true;

            var model = getModel(pid);
            return !model.UseVanillaSeasonLogic;
        }

        public static string getPidFromName(string name)
        {
            //This jank is necessary because at the first GetSeason call from a new location the moddata is null 
            name = name.Substring(BaseLocationKey.Length + 1);
            var split = name.Split("_");
            var pid = split.First();
            for (int i = 1; i < split.Length; i++) {
                if (long.TryParse(split[i], out var farmerId) && Game1.GetPlayer(farmerId) is not null)
                    break;

                pid += "_" + split[i];
            }

            if (pid == name)
                return null;

            return pid;
        }

        public static Season getSeason(GameLocation location)
        {
            if (!location.modData.TryGetValue(SeasonKey, out var season))
                season = Game1.season.ToString();

            return Enum.Parse<Season>(season);
        }

        public static KeyValuePair<string, string> getDoorModData(GameLocation location, Farmer owner)
        {
            foreach (var entry in owner.modData.Pairs)
                if (entry.Key.StartsWith(BaseFarmerPidKey) && location.Name == generateLocationKey(entry.Value, owner.UniqueMultiplayerID, getDoorIdFromFarmerPIDKey(entry.Key)))
                    return entry;

            return default(KeyValuePair<string, string>);
        }

        public static void incrementDayOfMonth(GameLocation location)
        {
            var day = getDayOfMonth(location);
            var season = getSeason(location);

            day++;

            if (day >= 29) {
                day = 1;
                season = season switch {
                    Season.Spring => Season.Summer,
                    Season.Summer => Season.Fall,
                    Season.Fall => Season.Winter,
                    Season.Winter or _ => Season.Spring,
                };
            }

            setDay(location, day.ToString());
            setSeason(location, season.ToString());
        }

        public static int getDayOfMonth(GameLocation location)
        {
            if (!location.modData.TryGetValue(DayOfMonthKey, out var day)) {
                day = Game1.dayOfMonth.ToString();
            }

            return int.Parse(day);
        }

        public static GameLocation createLocation(string pid, Farmer who, string doorId)
        {
            var model = getModel(pid);
            var locationKey = generateLocationKey(pid, who.UniqueMultiplayerID, doorId);
            var location = Game1.getLocationFromName(locationKey);
            if (location is null) {
                var createLocationData = getCreateLocationData(pid, who);
                location = Game1.CreateGameLocation(locationKey, createLocationData);

            }

            if (!Game1.locations.Contains(location))
                Game1.locations.Add(location);

            location.modData[OwnerKey] = who.UniqueMultiplayerID.ToString();
            location.modData[BaseFarmerPidKey] = pid; //.. yes there's two entries for the PID by mistake
            location.modData[DoorIdKey] = doorId;
            location.modData[PIDKey] = pid;

            var locationAsset = DataLoader.Locations(Game1.content);
            locationAsset.TryAdd(locationKey, model);

            location.DisplayName += $" ({who.displayName})";

            return location;
        }
        public static void checkHouseUpgraded(Farmer who, GameLocation loc, string pid)
        {
            var model = getModel(pid);
            var mapAsset = getMapAsset(who, model);

            if (loc.mapPath.Value != mapAsset) {
                loc.mapPath.Value = mapAsset;
                loc.reloadMap();

            }
        }

        /// <summary></summary>
        /// <param name="name">The PIF location to be removed</param>
        /// <param name="homeLocation">The players homeLocation. Provide this only when a player cabin is being destroyed</param>
        public static void removeLocation(string name, string homeLocation = null)
        {
            if (homeLocation is null && Game1.currentLocation.NameOrUniqueName == name)
                WarpToFarmHouse.HandleWarpToFarmHouse(null, null, Game1.player, default(Vector2));

            else if (Game1.currentLocation.NameOrUniqueName == name || Game1.currentLocation.NameOrUniqueName == homeLocation) {
                if (Game1.player.lastSleepLocation.Value == homeLocation)
                    Game1.player.lastSleepLocation.Value = null;

                BedFurniture.ApplyWakeUpPosition(Game1.player);
            }

            var location = Game1.getLocationFromName(name);
            if (location is not null)
                Game1.locations.Remove(location);

            var locationAsset = DataLoader.Locations(Game1.content);
            locationAsset.Remove(name);
        }
        public static string generateLocationKey(string pid, long farmerId, string doorId) => $"{BaseLocationKey}_{pid}_{farmerId}_{doorId}";
        public static string generateFarmerPIDKey(string doorId) => $"{BaseFarmerPidKey}_{doorId}";

        public static string getDoorIdFromFarmerPIDKey(string farmerPID) => farmerPID.Substring(generateFarmerPIDKey("").Length);

        public static bool isOwner(GameLocation location, Farmer who)
        {
            if (location.modData.ContainsKey(OwnerKey) && location.modData[OwnerKey] == who.UniqueMultiplayerID.ToString())
                return true;

            if (location.Name is null)
                return false;

            if (who.modData.Pairs.Any(e => e.Key.StartsWith(BaseFarmerPidKey) && location.Name.StartsWith(generateLocationKey(e.Value, who.UniqueMultiplayerID, ""))))
                return true;

            return false;
        }
    }
}
