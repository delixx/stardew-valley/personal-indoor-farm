﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm.Lib
{
    public enum DoorOwnerEnum
    {
        None = 0,
        Host = 1,
        CurrentPlayer = 2,
        Owner = 3,
        PlacedBy = 4,
    }
}
