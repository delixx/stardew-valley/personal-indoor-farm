﻿using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI.Enums;
using StardewValley.Locations;
using StardewValley.Objects;
using static PersonalIndoorFarm.ModEntry;
using HarmonyLib;

namespace PersonalIndoorFarm.Lib
{
    internal class DayUpdate
    {
        public static List<long> WasOnlineToday = new(); //Currently not multiplayer synced!!

        public static void Initialize()
        {
            Helper.Events.Specialized.LoadStageChanged += LoadStageChanged;
            Helper.Events.GameLoop.DayStarted += DayStarted;
            Helper.Events.Multiplayer.PeerConnected += PeerConnected;
            Helper.Events.GameLoop.ReturnedToTitle += ReturnedToTitle;

            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(GameLocation), nameof(GameLocation.DayUpdate), new[] { typeof(int) }),
                prefix: new HarmonyMethod(typeof(DayUpdate), nameof(DayUpdate.DayUpdate_Prefix))
            );
        }

        //Runs every DayEnding
        public static bool DayUpdate_Prefix(GameLocation __instance, ref int dayOfMonth)
        {
            if (__instance.NameOrUniqueName?.StartsWith(PersonalRoom.BaseLocationKey) != true)
                return true;

            if (!__instance.modData.ContainsKey(PersonalRoom.OwnerKey)) {
                Monitor.LogOnce($"PIF Room did not have OwnerKey modData during DayUpdate: {__instance.NameOrUniqueName}");
                return true;
            }

            if (!DayUpdate.WasOnlineToday.Any(e => e.ToString() == __instance.modData[PersonalRoom.OwnerKey]))
                return false;

            PersonalRoom.incrementDayOfMonth(__instance);
            dayOfMonth = PersonalRoom.getDayOfMonth(__instance);

            return true;
        }

        private static void ReturnedToTitle(object sender, StardewModdingAPI.Events.ReturnedToTitleEventArgs e)
        {
            if (Context.ScreenId > 0)
                return;

            RemoveAllPifLocations();
        }

        public static void RemoveAllPifLocations()
        {
            var locationAsset = DataLoader.Locations(Game1.content);

            var oldAssets = locationAsset.Where(el => el.Key.StartsWith(PersonalRoom.BaseLocationKey));
            foreach (var asset in oldAssets)
                locationAsset.Remove(asset.Key);

            var oldLocations = Game1.locations.Where(e => e.Name.StartsWith(PersonalRoom.BaseLocationKey));
            var i = oldLocations.Count();
            while (i-- > 0)
                Game1.locations.Remove(oldLocations.First());
        }

        private static void LoadStageChanged(object sender, StardewModdingAPI.Events.LoadStageChangedEventArgs e)
        {
            if (!Context.IsMainPlayer)
                return;

            if (e.NewStage != LoadStage.SaveLoadedBasicInfo)
                return;

            recreateAllLocations();
        }

        private static void DayStarted(object sender, StardewModdingAPI.Events.DayStartedEventArgs e)
        {
            if (Context.ScreenId > 0)
                return;

            if (Context.IsMainPlayer)
                fillWasOnline();
            else
                recreateAllLocations();
            var farmers = Game1.getAllFarmers();

            foreach (var farmer in farmers) {
                foreach (var doorId in Door.getDoorIds(farmer)) {
                    if (!farmer.modData.TryGetValue(PersonalRoom.generateFarmerPIDKey(doorId), out var pid))
                        continue;

                    var loc = Game1.getLocationFromName(PersonalRoom.generateLocationKey(pid, farmer.UniqueMultiplayerID, doorId));

                    if (loc is null) {
                        Monitor.LogOnce("Location does not exist: " + PersonalRoom.generateLocationKey(pid, farmer.UniqueMultiplayerID, doorId), LogLevel.Error);
                        continue;
                    }

                    PersonalRoom.checkHouseUpgraded(farmer, loc, pid);

                    loc.updateSeasonalTileSheets(); //Not sure if I need this, just in case : )
                }
            }
        }

        //I can't rely on Data/Locations directly, because I rely on a asset that doesn't get filled by CP before my Data/Locations asset edit
        //Also Game1.getLocationByName ignores unique names (mostly), so I'd have to create unique Data/Locations entries per location anyway, so it's whatever
        private static void recreateAllLocations()
        {
            var farmers = Game1.getAllFarmers();
            var locationAsset = DataLoader.Locations(Game1.content);

            var old = locationAsset.Where(el => el.Key.StartsWith(PersonalRoom.BaseLocationKey));
            foreach (var asset in old)
                locationAsset.Remove(asset.Key);


            foreach (var farmer in farmers) {
                foreach (var doorId in Door.getDoorIds(farmer)) {
                    if (!farmer.modData.TryGetValue(PersonalRoom.generateFarmerPIDKey(doorId), out var pid))
                        continue;

                    try {
                        PersonalRoom.createLocation(pid, farmer, doorId);
                    } catch (Exception ex) {
                        if (Context.ScreenId > 0)
                            continue;
                        Monitor.Log($"PIF ran into an error trying to create a room location.\n"
                            + $"If you save now the {doorId} room {pid} for the player {farmer.Name} will be permanently deleted.", LogLevel.Error);
                        Monitor.Log($"{ex}", LogLevel.Error);
                        farmer.modData.Remove(PersonalRoom.generateFarmerPIDKey(doorId));
                    }
                    
                }
            }

            Helper.GameContent.InvalidateCache(@"Data\Minecarts");
        }

        private static void PeerConnected(object sender, StardewModdingAPI.Events.PeerConnectedEventArgs e)
            => addWasOnline(e.Peer.PlayerID);


        private static void addWasOnline(long id)
        {
            if (!WasOnlineToday.Contains(id))
                WasOnlineToday.Add(id);
        }

        private static void fillWasOnline()
        {
            WasOnlineToday.Clear();
            var online = Game1.getOnlineFarmers();
            foreach (var farmer in online)
                addWasOnline(farmer.UniqueMultiplayerID);
        }
    }
}
