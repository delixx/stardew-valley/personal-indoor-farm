﻿using StardewModdingAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI.Events;
using static PersonalIndoorFarm.ModEntry;

namespace PersonalIndoorFarm.Lib
{
    internal class ModDataEvents
    {
        public static void Initialize()
        {
            Helper.Events.Multiplayer.PeerConnected += PeerConnected;
            Helper.Events.GameLoop.DayStarted += DayStarted;
            Helper.Events.GameLoop.DayEnding += DayEnding;
            Helper.Events.GameLoop.ReturnedToTitle += ReturnedToTitle;
            Helper.Events.Multiplayer.ModMessageReceived += ModMessageReceived;
        }

        private static void ReturnedToTitle(object sender, ReturnedToTitleEventArgs e)
        {
            if (Context.ScreenId > 0)
                return;

            ModData.DiscardInstance();
        }

        private static void DayEnding(object sender, DayEndingEventArgs e)
        {
            if (!Context.IsMainPlayer)
                return;

            ModData.SaveInstance();
        }

        private static void DayStarted(object sender, DayStartedEventArgs e)
        {
            RecreateModData();
        }

        private static void RecreateModData()
        {
            if (!Context.IsMainPlayer)
                return;

            Monitor.Log("Host recreating ModData for new day", DebugLogLevel);

            var modData = ModData.GetInstance(true);
            Helper.Multiplayer.SendMessage(modData, "ShareModData", new[] { ModManifest.UniqueID });
        }

        private static void PeerConnected(object sender, PeerConnectedEventArgs e)
        {
            ShareModDataWithConnectedPeer(e);
        }

        private static void ShareModDataWithConnectedPeer(PeerConnectedEventArgs e)
        {
            if (!Context.IsMainPlayer)
                return;

            Monitor.Log($"Host sharing ModData with new peer: {e.Peer.PlayerID}", DebugLogLevel);

            var modData = ModData.GetInstance();
            Helper.Multiplayer.SendMessage(modData, "ShareModData", new[] { ModManifest.UniqueID }, new[] { e.Peer.PlayerID });
        }

        private static void ModMessageReceived(object sender, ModMessageReceivedEventArgs e)
        {
            switch (e.Type) {
                case "ShareModData":
                    ReceivedShareModData(e); break;
            }
        }

        private static void ReceivedShareModData(ModMessageReceivedEventArgs e)
        {
            if (Context.ScreenId > 0)
                return;

            Monitor.Log($"{GetPlayerDebugName()} Received ModData", DebugLogLevel);

            ModData.SetInstance(e.ReadAs<ModData>());
        }

    }
}
