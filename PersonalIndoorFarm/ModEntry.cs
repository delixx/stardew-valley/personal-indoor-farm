﻿using PersonalIndoorFarm.Lib;
using StardewModdingAPI;
using StardewValley;
using StardewValley.TerrainFeatures;
using System;
using System.Collections.Generic;

namespace PersonalIndoorFarm
{
    public class ModEntry : Mod
    {
        public static Mod ModInstance;
        public static ModConfig Config;

        public static new IMonitor Monitor;
        public static new IModHelper Helper;
        public static new IManifest ModManifest;

        public static LogLevel DebugLogLevel = LogLevel.Trace;

        public override void Entry(IModHelper helper)
        {
            ModInstance = this;
            Config = helper.ReadConfig<ModConfig>();

            Monitor = ModInstance.Monitor;
            Helper = ModInstance.Helper;
            ModManifest = ModInstance.ModManifest;

            API.Main.Initialize();
            Lib.Main.Initialize();

#if Debug
            DebugLogLevel = LogLevel.Debug;
#endif

            Helper.ConsoleCommands.Add("pif", "", Commands);
        }

        public static string GetPlayerDebugName() => $"{(Context.IsOnHostComputer ? "P" + Context.ScreenId : "NotOnHostComputer")} {Game1.player.UniqueMultiplayerID}";

        private static void PrintValidCommands() =>
            Monitor.Log("Valid Commands: help, day, season", LogLevel.Info);

        private static void PrintHelp() =>
            Monitor.Log(
                "Valid Commands:\n" +
                "DAY <DAY>          Changes the rooms PIF day into <DAY>.\n" +
                "SEASON <SEASON>    Changes the rooms PIF season into <SEASON> (spring/summer/fall/winter)"
                    , LogLevel.Info);


        private static void Commands(string command, string[] args)
        {
            if (args.Length == 0) {
                PrintValidCommands();
                return;
            }

            switch (args[0].ToLower()) {
                case "help":
                    PrintHelp(); break;

                case "day":
                    DayCommand(args); break;

                case "season":
                    SeasonCommand(args); break;
            }
        }

        private static void DayCommand(string[] args)
        {
            if (args.Length < 2) {
                Monitor.Log("Missing <DAY> argument", LogLevel.Error);
                return;
            }

            if (!int.TryParse(args[1], out var day) || day < 0 || day > 28) {
                Monitor.Log($"{args[1]} is not a number between 0 and 28", LogLevel.Error);
                return;
            }

            var location = Game1.currentLocation;
            if (location.NameOrUniqueName?.StartsWith(PersonalRoom.BaseLocationKey) == false) {
                Monitor.Log("This command can only be used in PIF rooms", LogLevel.Error);
                return;
            }
#if Release
            if (!PersonalFarm.isOwner(location, Game1.player)) {
                Monitor.Log("You must be the room owner to use this command", LogLevel.Error);
                return;
            }
#endif
            PersonalRoom.setDay(location, day.ToString());
            Monitor.Log($"Set day to {day} for {location.NameOrUniqueName}", LogLevel.Info);
        }

        private static void SeasonCommand(string[] args)
        {
            if (args.Length < 2) {
                Monitor.Log("Missing <SEASON> argument", LogLevel.Error);
                return;
            }


            if (!Enum.TryParse<Season>(args[1].Trim(), true, out var season)) {
                Monitor.Log($"{args[1]} is not a valid season. Must be spring/summer/fall/winter", LogLevel.Error);
                return;
            }

            var location = Game1.currentLocation;
            if (location.NameOrUniqueName?.StartsWith(PersonalRoom.BaseLocationKey) == false) {
                Monitor.Log("This command can only be used in PIF rooms", LogLevel.Error);
                return;
            }
#if Release
            if (!PersonalFarm.isOwner(location, Game1.player)) {
                Monitor.Log("You must be the room owner to use this command", LogLevel.Error);
                return;
            }
#endif
            PersonalRoom.setSeason(location, season.ToString());
            Monitor.Log($"Set season to {season} for {location.NameOrUniqueName}", LogLevel.Info);
        }
    }
}
