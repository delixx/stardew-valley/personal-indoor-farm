﻿using Microsoft.Xna.Framework;
using PersonalIndoorFarm.Lib;
using PersonalIndoorFarm.Lib.SelectionMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalIndoorFarm
{
    public class ModConfig
    {
        public string OwnerFarmhouse { get; set; }
        public string OwnerOutside { get; set; }
        public string SelectionMenuStyle { get; set; }
        public Color LockedDoorColor { get; set; }
        public Color LockedWhenOfflineDoorColor { get; set; }
        public Color UnlockedDoorColor { get; set; }
        public ModConfig()
        {
            OwnerFarmhouse = DoorOwnerEnum.Owner.ToString();
            OwnerOutside = DoorOwnerEnum.PlacedBy.ToString();
            SelectionMenuStyle = SelectionMenuStyles.Tarot.ToString();
            LockedDoorColor = Color.Red * 0.6f;
            LockedWhenOfflineDoorColor = Color.Yellow * 0.6f;
            UnlockedDoorColor = Color.ForestGreen * 0.6f;
        }
    }
}
